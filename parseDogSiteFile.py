from re import search, compile
from os import listdir




def retrieveDrugScore (path_file_score, s_PDB) :
    
    if path_file_score == "" : 
        return "NA", "NA"  
    filin = open (path_file_score, "r")
    l_line = filin.readlines ()
    filin.close ()
    for line_file in l_line : 
        if search (s_PDB, line_file) : 
            return line_file.split (" ")[1], line_file.strip().split (" ")[-1]
    return "NA", "NA"

def retrieveScoreDogsite (path_folder, s_name_pocket) :
    l_file = listdir (path_folder)
    for  file_poc in l_file : 
        if search ("PocXls_", file_poc) or search ("SpocXls_", file_poc) and search (".txt2$", file_poc) :
            score = retrieveScoreFile (path_folder +"/" +  file_poc, s_name_pocket)
            if score != "NA" : 
                return score
    return "NA"
            

def retrieveScoreFile (path_file_pocket, s_name_pocket) : 
    filin = open (path_file_pocket, "r")
    l_lines = filin.readlines ()
    filin.close ()

    for line_file in l_lines :
        #print line_file 
        if search (s_name_pocket, line_file) :
            #print line_file.split ("\t")
            return line_file.split ("\t")[5].strip()
    return "NA"


def retrieveNamePocket (path_in):
    path_out = path_in.split ("_")[-1][:-4]
    return path_out



def manageFileDOGSITE (path_folder, PDB_ID) : 

    path_html, path_pocket, path_subpocket = searchFilePocket(path_folder, PDB_ID)
    html_parsed = parseHTMLDogsite (path_html)
    changeScore (html_parsed, path_pocket)
    changeScore (html_parsed, path_subpocket)
    

def changeScore (html_parsed, path_filin) : 
    
    filout = open (path_filin + "2", "w")
    regex = compile('[0-9]+\.[0-9]+')
    filin = open (path_filin, "r")
    list_line = filin.readlines ()
    filin.close ()
    
    for lin in list_line :
        l_float_find = regex.findall (lin)
        if len (l_float_find) < 5 : 
            continue
        volume = l_float_find[0]
        for pocket in html_parsed : 
            if volume == pocket["Volume"] : 
                split_line = lin.split ("\t")
                filout.write ("%s\t%s\t%s\t%s\t%s\t%s\n"%(split_line[0], pocket["Volume"], pocket["Surface"], pocket["Lipo surface"], pocket["Depth"], pocket["Score"]))
    filout.close ()
    

def parseHTMLDogsite (path_html) : 
    
    d_out = {}
    filin = open (path_html, "r")
    content_file = filin.read ()
    filin.close ()
    regex = compile('[0-9]+\.[0-9]+')
    
    pocket_group = content_file.split ("<tr>")
    for element in pocket_group : 
        
        l_float_find = regex.findall (element)
        if len (l_float_find) == 5 :
            name = element.split ("onclick=\"window.open('results")[-1].split ("_")[3].split (".html")[0]
            d_out[name] = {}
            d_out[name]["Volume"]  = l_float_find[0]
            d_out[name]["Surface"]  = l_float_find[1]
            d_out[name]["Lipo surface"]  = l_float_find[2]
            d_out[name]["Depth"]  = l_float_find[3]
            d_out[name]["Score"]  = l_float_find[4]
            
    return d_out










