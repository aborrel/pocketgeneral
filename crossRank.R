#!/usr/bin/env Rscript

# BORREL Alexandre
# 07-2014


args <- commandArgs(TRUE)

p_filin = args[1]



d = read.table (p_filin, sep ="\t", header = TRUE)
png (paste(p_filin, ".png", sep = ""), 2400, 1000)
plot (seq(1,dim(d)[1]), d[,1], col = "blue", main = "", type = "l", lwd = 1, cex.axis = 2.5)
lines (seq(1,dim(d)[1]), d[,2], col = "green", lwd = 1)
lines (seq(1,dim(d)[1]), d[,3], col = "violet", lwd = 1)
segments (1:dim(d)[1],0,1:dim(d)[1],dim(d)[1])
legend ("topleft",legend = c("prox4","fpocket","DoGSite"), col = c("blue","green","violet"), pch = c(16,16,16))
lines (seq(1,dim(d)[1]), apply (d,1,mean), lwd = 3)

dev.off ()

print ("**************")
print (apply(d,1,mean))
print ("**************")
print (sort (apply(d,1,mean)))
