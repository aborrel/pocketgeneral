"""
BORREL Alexandre
04-2012
run analysis
"""
# personal modules
import pathDirectory
import parseOutLDA
import runOtherProg
import tool
import writeFiles

# global modules
from numpy import arange
import os
from re import search, sub
from itertools import combinations
from copy import deepcopy
from shutil import copyfileobj
import time



def optimizeSVM (path_dir, path_file_global, path_file_train, path_file_test, begin = 0.4, end = 1.01, step = 0.01):
    
    path_dir_result = pathDirectory.generatePath(path_dir + "ElimCor/")
    
    path_filout_sp_sen = path_dir_result + "best_valueSpSen"
    path_filout_acc = path_dir_result + "best_acc"
    filout_sensibility_specificite = open (path_filout_sp_sen, "w")
    filout_acc = open (path_filout_acc, "w")
    for elimcor in arange (begin, end, step) :
        print elimcor, "-> test elimcor"
        path_file_result = runOtherProg.SVM(path_file_global, path_file_train, path_file_test, elimcor=elimcor, debug=1)
        
        try:
            nb_descriptor, acc_loo, acc_train, acc_test = parseOutLDA.retrieveAllAccuracy(path_file_result)
            filout_acc.write (str (elimcor) + "\t" + str (nb_descriptor) + "\t" + str (acc_loo) + "\t" + str (acc_train) + "\t" + str(acc_test) + "\n")
        except:
            pass
        
        try :
            nb_descriptor, accuracy, precision, recall, sensibility, specificity = parseOutLDA.retrieveQualityLOO(path_file_result)
            filout_sensibility_specificite.write (str (elimcor) + "\t" + str(nb_descriptor) + "\t" + str(accuracy) + "\t" + str(precision) + "\t" + str (recall) + "\t" + str (sensibility) + "\t" + str (specificity) + "\n")
        except :
            pass
        
        os.system ("rm " + path_file_result)
        
    filout_sensibility_specificite.close ()
    filout_acc.close ()
    runOtherProg.RplotElimcor(path_filout_sp_sen, 1)
    runOtherProg.RplotElimcor(path_filout_acc, 0)
    
    return bestElimAccuracy (path_filout_acc)




def optimizeLDA (path_dir, path_descriptors, begin = 0.3, end = 1.01, step = 0.01):
    
    path_dir_result = pathDirectory.generatePath(path_dir + "ElimCor/")
    
    path_filout_sp_sen = path_dir_result + "best_valueSpSen"
    path_filout_acc = path_dir_result + "best_acc"
    filout_sensibility_specificite = open (path_filout_sp_sen, "w")
    filout_acc = open (path_filout_acc, "w")
    for elimcor in arange (begin, end, step) :
        runOtherProg.lda(path_descriptors + "_loo", path_filin2=0, path_filout=path_descriptors + "_" + str (elimcor), leave_one_out=1, name_barplot="0", elimcor=elimcor, graph=0, debug=1)
        runOtherProg.lda(path_descriptors + "_train", path_filin2=path_descriptors + "_test", path_filout=path_descriptors + "_" + str (elimcor), leave_one_out=0, name_barplot="0", elimcor=elimcor, graph=0, debug=1)
        
        try:
            nb_descriptor, acc_loo, acc_train, acc_validation = parseOutLDA.retrieveAllAccuracy(path_descriptors + "_" + str (elimcor))
            filout_acc.write (str (elimcor) + "\t")
            filout_acc.write (str (nb_descriptor) + "\t" + str (acc_loo) + "\t" + str (acc_train) + "\t" + str(acc_validation) + "\n")
        except:
            pass
        
        try :
            nb_descriptor, accuracy, precision, recall, sensibility, specificity = parseOutLDA.retrieveQualityLOO(path_descriptors + "_" + str (elimcor))
            filout_sensibility_specificite.write (str (elimcor) + "\t")
            filout_sensibility_specificite.write (str(nb_descriptor) + "\t" + str(accuracy) + "\t" + str(precision) + "\t" + str (recall) + "\t" + str (sensibility) + "\t" + str (specificity) + "\n")
        except :
            pass
        
        
        os.system ("rm " + path_descriptors + "_" + str (elimcor))
        
    filout_sensibility_specificite.close ()
    filout_acc.close ()
    runOtherProg.RplotElimcor(path_filout_sp_sen, 1)
    runOtherProg.RplotElimcor(path_filout_acc, 0)
    
    return bestElimAccuracy (path_filout_acc)


def bestElimCorSpeSen (path_filin):
    
    filin = open (path_filin, "r")
    list_lines = filin.readlines ()
    filin.close ()
    
    min_value = 100
    out_value = 1
    for line_file in list_lines : 
        element_line = line_file.split ("\t")
        elimcor = float (element_line[0])
        specificity = float (element_line[-1])
        sensibility = float (element_line[-2])
        ecart = (1-specificity) + (1-sensibility)
        if ecart < min_value :
            out_value = elimcor
            min_value = ecart
    
    return out_value
    
def bestElimAccuracy (path_filin):
    
    filin = open (path_filin, "r")
    list_lines = filin.readlines ()
    filin.close ()
    
    min_value = 100
    out_value = 1
    for line_file in list_lines : 
        element_line = line_file.split ("\t")
        elimcor = float (element_line[0])
        acc_loo = float (element_line[3])
        acc_train = float (element_line[3])
        acc_test = float (element_line[4])
        
#        if acc_loo == 1 : 
#            acc_loo = 0 
#        if acc_test == 1 : 
#            acc_test = 0
#        if acc_train == 1 : 
#            acc_train = 0
        
        ecart = (1-acc_train) * 0.8 + (1-acc_test)* 0.7  + (1-acc_loo)*0.5
        if ecart < min_value :
            out_value = elimcor
            min_value = ecart
    
    return out_value

def onlyACCLoo (path_filin):
    """
    Select best elimcor for up accuracy
    args: -> path filin
    return: -> best elimcor
    """
    
    acc_temp = 0
        
    for elm in  arange (0.3, 1.1, 0.1) : 
        runOtherProg.lda(path_filin, path_filin2=0, path_filout=path_filin + "_loo", leave_one_out=1, name_barplot="0", elimcor=elm, graph=0, debug=0)
        try : nb_descriptor, accuracy, precision, recall, sensibility, specificity= parseOutLDA.retrieveQualityLOO(path_filin + "_loo")
        except : continue
        if accuracy > acc_temp : 
            out_elim = elm
            acc_temp = accuracy
    
    return out_elim
    
    
def retrieveDescriptorForLDAModel (dico_desc, path_file_train, path_file_test, path_dir, clean = 0, nb_desc_model = 3, criteria_train = 100, criteria_loo = 0, criteria_CV = 0, criteria_test = 0, debug = 0):
    """
    Retrieve list of descriptors 
    args : -> path file train
           -> path file test
    return: -> list descriptors
    """
    # lot of fonction -> elimcor (not use now)
    # selection with all model
 
    ###### short cut 
    p_all_models = path_dir + "tempSelected"
    if clean == 1 : 
        if os.path.exists(p_all_models) : 
            os.remove(p_all_models)
        else : 
            pass
    if not os.path.exists(p_all_models) or os.path.getsize(p_all_models ) < 1000 :
        p_all_models = runOtherProg.descriptorSelectionLDA (path_file_train, path_file_test,  p_all_models, nb_desc_model)
    else : 
        # path exist
        pass

    list_descriptor = selectBestModelAmongBestModel (dico_desc, p_all_models, criteria_train = criteria_train, criteria_loo = criteria_loo, criteria_CV = criteria_CV, criteria_test = criteria_test)
 
    return list_descriptor
 
def retrieveDescriptorForSVMModel (dico_descriptor, path_dir, nb_descriptor = 3, debug = 0):
    """
    Retrieve list of descriptors 
    args : -> path file train
           -> path file test
    return: -> list descriptors
    """
    
    l_descriptors = tool.listDescriptor(dico_descriptor["Druggable"])
    
    l_combi_desc = list(combinations(l_descriptors, nb_descriptor))
    
    i_combi = 0 
    nb_combi = len (l_combi_desc) 
#     print nb_combi
#     nb_combi = 100
    
    l_file_SVM = []
    while i_combi < nb_combi : 
        
        l_p_file = writeFiles.specificDescriptorbyData(dico_descriptor, list(l_combi_desc[i_combi]), path_dir + str(i_combi), debug = 0)
        
# # # # # #         # short cut
# # # # # #         return os.path.split(l_p_file[0])[0] + "/tempSelected"
# # # # # #         #
        
        out_file = runOtherProg.descriptorSelectionSVM(l_p_file[0], l_p_file[1], os.path.split(l_p_file[0])[0] + "/" +  str(i_combi) + ".res")
        l_file_SVM.append (out_file)
        
        i_combi = i_combi + 1
        # break script
        if i_combi%100 == 0 : 
            #time.sleep(420) # Helsinki computer
            time.sleep(660) # Paris computer
        

            
#     filend = open(os.path.split(l_p_file[0])[0] + "/tempSelected", "w")
#     for res_svm in l_file_SVM:
#         copyfileobj(open(res_svm, 'r'), filend)
#    
#     filend.close()
#       
#     # clean respertory
#     os.system ("rm " + os.path.split(l_p_file[0])[0] + "/*.res")
#     os.system ("rm " + os.path.split(l_p_file[0])[0] + "/*train")
#     os.system ("rm " + os.path.split(l_p_file[0])[0] + "/*test")
     
    return os.path.split(l_p_file[0])[0] + "/tempSelected"


def countDescriptorFile (dico_desc, path_filin,  criteria_train = 100, criteria_loo = 0, criteria_CV = 0, criteria_test = 0, type_learning = "LDA") :
    """
    Count descriptor in list of model
    args -> path file with descriptor
         -> number of best model selected
    return: -> dictionary count
            -> combinaison descriptor
            -> best model
    """
    p_filout = os.path.dirname (path_filin) + "/acc_mcc"
    filout = open (p_filout, "w")
    l_d_model = []
    
    criterion_mcc = 0
    filin = open (path_filin, "r")
    filin_read = filin.read ()
    filin.close ()
    list_model = filin_read.split ("[1] \"**********************************************************************\"\n") 

    for model in list_model : 
        l_desc_acc_mcc_spse = parseOutLDA.retrieveDescriptorAccModel(model.split ("\n"))
        if l_desc_acc_mcc_spse : 
            d_model = {}
            l_desc = l_desc_acc_mcc_spse [0]
            l_acc = l_desc_acc_mcc_spse [1]
            l_mcc = l_desc_acc_mcc_spse [2]
            l_sp_se = l_desc_acc_mcc_spse[3]
            l_des_implication = l_desc_acc_mcc_spse[4]
            
            if l_mcc[0] == 'NaN' or l_mcc[1] == 'NaN' or l_mcc[2] == 'NaN' or l_mcc[3] == 'NaN' or l_mcc[4] == 'NaN' or l_mcc[5] == 'NaN' or l_mcc[6] == 'NaN': 
                continue
            # implement model
            d_model["acc LOO"] = float (l_acc[0])
            d_model["acc TRAIN"] = float (l_acc[1])
            d_model["acc TEST"] = float (l_acc[2])
            d_model["acc CV train"] = float (l_acc[3])
            d_model["acc CV train SD"] = float (l_acc[4])
            d_model["acc CV test"] = float (l_acc[5])
            d_model["acc CV test SD"] = float (l_acc[6])
            
            d_model["descriptors"] = l_desc
            d_model["implication"] = l_des_implication
            
            d_model["mcc LOO"] = float (l_mcc[0])
            d_model["mcc TRAIN"] = float (l_mcc[1])
            d_model["mcc TEST"] = float (l_mcc[2])
            try :d_model["mcc CV train"] = float (l_mcc[3])
            except : d_model["mcc CV train"] = 0.0
            try :d_model["mcc CV train SD"] = float (l_mcc[4])
            except : d_model["mcc CV train SD"] = 0.0
            try : d_model["mcc CV test"] = float (l_mcc[5])
            except : d_model["mcc CV test"] = 0.0
            try : d_model["mcc CV test SD"] = float (l_mcc[6])
            except : d_model["mcc CV test SD"] = 0.0
            
            d_model["se LOO"] = float (l_sp_se[0])
            d_model["se TRAIN"] = float (l_sp_se[2])
            d_model["se TEST"] = float (l_sp_se[4])
            d_model["se CV train"] = float (l_sp_se[6])
            d_model["se CV train SD"] = float (l_sp_se[7])
            d_model["se CV test"] = float (l_sp_se[8])
            d_model["se CV test SD"] = float (l_sp_se[9])
            
            
            d_model["sp LOO"] = float (l_sp_se[1])
            d_model["sp TRAIN"] = float (l_sp_se[3])
            d_model["sp TEST"] = float (l_sp_se[5])
            d_model["sp CV train"] = float (l_sp_se[10])
            d_model["sp CV train SD"] = float (l_sp_se[11])
            d_model["sp CV test"] = float (l_sp_se[12])
            d_model["sp CV test SD"] = float (l_sp_se[13])
            
            # criteria
            d_model["criterion mcc"] = d_model["mcc TRAIN"] + d_model["mcc LOO"] # MCC test - 2
            d_model["criterion acc"] = float (l_acc[0]) + float (l_acc[1])
            
            l_d_model.append (d_model)
            
            # control rates 
            filout.write ("\t".join(l_acc) + "\t" + "\t".join(l_mcc) + "\n")
            
            # select best model, with one loop
            if d_model["criterion mcc"] > criterion_mcc : 
                best_model = l_desc
                #tool.percentageR(l_desc)
                criterion_mcc = d_model["criterion mcc"]

    
    # histogram to control the score selected -> distribution
    filout.close ()
    runOtherProg.plotQualityMCCACC (p_filout)

    # object programation -> change directly the list of model -> limit the number
    selectBestModel (dico_desc, l_d_model, os.path.dirname (path_filin), criteria_train = criteria_train, criteria_loo = criteria_loo, criteria_CV = criteria_CV, criteria_test = criteria_test,type_learning=type_learning)
    
    # dico count            
    dico_count = countDescriptor (l_d_model)
    dico_count_combi = countDescriptorCombi (l_d_model)
    dico_desc_ranking = countRakingDesc (l_d_model)    
#     tool.printDico(dico_count)
#     print (dico_count_combi)
    return [dico_count, dico_count_combi, dico_desc_ranking, best_model]





def countDescriptor (l_model) : 

    d_count = {}
    
    for model in l_model : 
        for desc in model["descriptors"] : 
            if not desc in d_count.keys () : 
                d_count[desc] = 1
            else : 
                d_count[desc] =  d_count[desc] + 1
    


    return d_count


def countDescriptorCombi (l_model) : 
    
    dico_count = {}
    
    for model in l_model : 
        list_descriptor = model["descriptors"]
        nb_descriptor = len (list_descriptor)
        
        i = 0
        while i < nb_descriptor - 1 : 
            #print i, "i"
            j = i + 1
            while j< nb_descriptor : 
                #print j, "j"
                if not list_descriptor[i] in dico_count.keys () :
                    if not list_descriptor[j] in  dico_count.keys () :
                        dico_count[list_descriptor[i]] = {}
                        dico_count[list_descriptor[i]][list_descriptor[j]] = 1
                    else :
                        if not list_descriptor[i] in dico_count[list_descriptor[j]].keys () : 
                            dico_count[list_descriptor[j]][list_descriptor[i]] = 1    
                        else :             
                            dico_count[list_descriptor[j]][list_descriptor[i]] = dico_count[list_descriptor[j]][list_descriptor[i]] + 1
                else : 
                    if not list_descriptor[j] in dico_count[list_descriptor[i]].keys () :
                        dico_count[list_descriptor[i]][list_descriptor[j]] = 1
                    else :                
                        dico_count[list_descriptor[i]][list_descriptor[j]] = dico_count[list_descriptor[i]][list_descriptor[j]] + 1
                j = j + 1
            i = i + 1

    return dico_count
                

def countRakingDesc (l_d_model)  : 
    
    d_out = {}
    d_out["descriptors"] = []
    nb_desc = len (l_d_model[0]["descriptors"])
    
    print nb_desc, "nb_des"
    print l_d_model[0]["descriptors"]
    
    i = 0
    while i < nb_desc : 
        d_out[i] = {}
        i = i + 1
    
    for model in l_d_model : 
        l_sort = deepcopy(model["implication"])
        l_sort.sort (reverse=True)
        
        i = 0
        while i < nb_desc : 
            i_val = model["implication"].index (l_sort[i])
                
            if not model["descriptors"][i_val] in d_out[i].keys () : 
                d_out[i][model["descriptors"][i_val]] = 1
            else : 
                d_out[i][model["descriptors"][i_val]] = d_out[i][model["descriptors"][i_val]] + 1
 
            if not model["descriptors"][i_val] in d_out["descriptors"] : 
                d_out["descriptors"].append (model["descriptors"][i_val])
            else : 
                pass
 
            i = i + 1
            
             
    return d_out
    
    
    



def selectBestModelAmongBestModel (dico_desc, path_file_every_best_model, criteria_train = 100, criteria_loo = 0, criteria_CV = 0, criteria_test = 0, type_learning = "LDA") : 
    
    # open file with model and select first model
    table_count =  countDescriptorFile (dico_desc, path_file_every_best_model, criteria_train = criteria_train, criteria_loo = criteria_loo, criteria_CV = criteria_CV, criteria_test = criteria_test,  type_learning = type_learning)
    
    path_file_count = writeFiles.writeCount(table_count[0], path_file_every_best_model + str (criteria_train) + "_" + str (criteria_loo) + "_" + str(criteria_CV) + "_" + str (criteria_test) + "_countTable")
    path_file_count_combi = writeFiles.writeCountCombi(table_count[1], path_file_every_best_model +  str (criteria_train) + "_" + str (criteria_loo) + "_" + str(criteria_CV) + "_" + str (criteria_test) +"_countCombiTable")
    
    p_file_implication = writeFiles.descImplicationLDA (table_count[2], path_file_every_best_model +  str (criteria_train) + "_" + str (criteria_loo) + "_" + str(criteria_CV) + "_" + str (criteria_test) +"_countImplication")
    
    runOtherProg.histNameProtein(path_file_count)
    runOtherProg.histNameProtein(path_file_count_combi)
    runOtherProg.pieDescriptor (path_file_count)
    runOtherProg.pieDescriptor (p_file_implication)
    
    # best model in output table count 
    return table_count[3]
    
    
    
def  selectBestModel (dico_descriptor, l_model, p_dir, criteria_train = 100, criteria_loo = 0, criteria_CV = 0, criteria_test = 0, type_learning = "LDA", debug = 1) : 
    
    if type (criteria_train) == list : 
        
        subname_file = "_".join(criteria_train)
    else : 
        subname_file = str(criteria_train) + "_" + str (criteria_loo) + "_" + str (criteria_CV) + "_" + str (criteria_test)
    
    p_filout_histo = p_dir + "/score_MCC_hist_" + str (subname_file)
    p_filout_model = p_dir + "/best_" + str(subname_file) + "_model"
    p_filout_score = p_dir + "/score_criterion_" + str(subname_file)
    p_resume = p_dir + "/summary_" + str(subname_file)
    p_dir_model = pathDirectory.generatePath(p_dir + "/BestModels" + str (subname_file)+"/")
    os.system("rm -f " + p_dir_model + "*")
    
    filout_histo = open(p_filout_histo, "w")
    filout_model = open(p_filout_model, "w")
    filout_score = open (p_filout_score, "w")
    
    if type (criteria_train) == int : 
        # criterion selected
        l_criterion = []
        # histogram score MCC
        
        nb_model = len (l_model)
        if debug : print len (l_model)
        i = 0
        while i < nb_model : 
            l_criterion.append( l_model[i]["criterion mcc"])
            i = i + 1
        
        if debug : print len(l_model)
        
        l_criterion.sort(reverse=True)
        print l_criterion[0:13]
        
    
        if len (l_criterion) > criteria_train : 
            criterion = l_criterion[criteria_train+1]
        else : 
            criterion = l_criterion[-1]
        
        i = 0
        nb_model = len (l_model)
        
        if debug :print criterion, "criterion"
        if debug : print len (l_model) 
        
        while i < nb_model : 
            if l_model[i]["criterion mcc"] < criterion : 
                del l_model[i]
                nb_model = nb_model - 1
                continue
            else :
                # write file with n best models
                filout_model.write ("************************************************\n")
                filout_model.write ("----".join(l_model[i]["descriptors"]) + "\n")
                filout_model.write ("************--ACCURACY--**********\n")
                filout_model.write (str(l_model[i]["acc LOO"]) + "---" + str(l_model[i]["acc TRAIN"]) + "---" + str(l_model[i]["acc TEST"]) + "---" + str(l_model[i]["acc CV train"]) + "---" + str(l_model[i]["acc CV train SD"]) + "---" + str(l_model[i]["acc CV test"]) + "---" + str(l_model[i]["acc CV test SD"]) + "\n")
                filout_model.write ("************--SP SE--**********\n")
                filout_model.write (str(l_model[i]["sp LOO"]) + "---" + str(l_model[i]["se LOO"]) + "---" + str(l_model[i]["sp TRAIN"]) + "---" + str(l_model[i]["se TRAIN"]) + "---" + str(l_model[i]["sp TEST"]) + "---" + str(l_model[i]["se TEST"]) + "---" + str(l_model[i]["sp CV train"]) + "---" + str(l_model[i]["sp CV train SD"]) + "---" + str(l_model[i]["sp CV test"]) + "---" +  str(l_model[i]["sp CV test SD"]) + "---" + str(l_model[i]["se CV train"]) + "---" + str(l_model[i]["se CV train SD"]) + "---" + str(l_model[i]["se CV test"]) + "---" +  str(l_model[i]["se CV test SD"]) + "\n")
                filout_model.write ("************--MCC--**********\n")
                filout_model.write (str(l_model[i]["mcc LOO"]) + "---" + str(l_model[i]["mcc TRAIN"]) + "---" + str(l_model[i]["mcc TEST"]) + "---" + str(l_model[i]["mcc CV train"]) + "---" + str(l_model[i]["mcc CV train SD"]) + "---" + str(l_model[i]["mcc CV test"]) + "---" + str(l_model[i]["mcc CV test SD"]) +  "\n")
                
                filout_histo.write (str (l_model[i]["criterion mcc"]) + "\t" + str(l_model[i]["mcc TEST"]) + "\n")
                filout_score.write ("M" + str (i + 1) + "\t" + str (l_model[i]["criterion mcc"]) + "\t" + str(l_model[i]["mcc LOO"]) + "\t" + str(l_model[i]["mcc TEST"]) + "\t" +  str(l_model[i]["mcc TRAIN"]) + "\t" + str(l_model[i]["acc LOO"]) + "\t" + str(l_model[i]["acc TEST"]) + "\t" +  str(l_model[i]["acc TRAIN"])+ "\t" +  str(l_model[i]["sp TEST"]) + "\t" +  str(l_model[i]["se TEST"]) + "\t" + str(l_model[i]["mcc CV test"]) +  "\n" )
                # make model in folder
                path_desc = writeFiles.specificDescriptorbyData(dico_descriptor, l_model[i]["descriptors"], p_dir_model + str(i) + "_")[0]
                if type_learning == "LDA" : 
                    runOtherProg.runSaveModelLDA(path_desc)
                elif type_learning == "SVM" :
                    runOtherProg.runSaveModelSVM(path_desc)
                
                i = i + 1 
    
    elif type (criteria_train) == float : 
        
        i = 0
        nb_model = len (l_model)
        
        if debug :print criteria_train, "criterion"
        if debug : print len (l_model) 
        
        while i < nb_model : 
            if l_model[i]["mcc TRAIN"] <= criteria_train : 
                del l_model[i]
                nb_model = nb_model - 1
                continue
            # select on mcc LOO
            if l_model[i]["mcc LOO"] <= criteria_loo :
                del l_model[i]
                nb_model = nb_model - 1
                continue
            
            if l_model[i]["mcc CV test"] <= criteria_CV :
                del l_model[i]
                nb_model = nb_model - 1
                continue
            
            if l_model[i]["mcc TEST"] <= criteria_test :
                del l_model[i]
                nb_model = nb_model - 1
                continue
            
                
            else :
                # write file with n best models
                filout_model.write ("************************************************\n")
                filout_model.write ("----".join(l_model[i]["descriptors"]) + "\n")
                filout_model.write ("************--ACCURACY--**********\n")
                filout_model.write (str(l_model[i]["acc LOO"]) + "---" + str(l_model[i]["acc TRAIN"]) + "---" + str(l_model[i]["acc TEST"]) + "---" + str(l_model[i]["acc CV train"]) + "---" + str(l_model[i]["acc CV train SD"]) + "---" + str(l_model[i]["acc CV test"]) + "---" + str(l_model[i]["acc CV test SD"]) + "\n")
                filout_model.write ("************--SP SE--**********\n")
                filout_model.write (str(l_model[i]["sp LOO"]) + "---" + str(l_model[i]["se LOO"]) + "---" + str(l_model[i]["sp TRAIN"]) + "---" + str(l_model[i]["se TRAIN"]) + "---" + str(l_model[i]["sp TEST"]) + "---" + str(l_model[i]["se TEST"]) + "---" + str(l_model[i]["sp CV train"]) + "---" + str(l_model[i]["sp CV train SD"]) + "---" + str(l_model[i]["sp CV test"]) + "---" +  str(l_model[i]["sp CV test SD"]) + "---" + str(l_model[i]["se CV train"]) + "---" + str(l_model[i]["se CV train SD"]) + "---" + str(l_model[i]["se CV test"]) + "---" +  str(l_model[i]["se CV test SD"]) + "\n")
                filout_model.write ("************--MCC--**********\n")
                filout_model.write (str(l_model[i]["mcc LOO"]) + "---" + str(l_model[i]["mcc TRAIN"]) + "---" + str(l_model[i]["mcc TEST"]) + "---" + str(l_model[i]["mcc CV train"]) + "---" + str(l_model[i]["mcc CV train SD"]) + "---" + str(l_model[i]["mcc CV test"]) + "---" + str(l_model[i]["mcc CV test SD"]) +  "\n")
                
                filout_histo.write (str (l_model[i]["criterion mcc"]) + "\t" + str(l_model[i]["mcc TEST"]) + "\n")
                filout_score.write ("M" + str (i + 1) + "\t" + str (l_model[i]["criterion mcc"]) + "\t" + str(l_model[i]["mcc LOO"]) + "\t" + str(l_model[i]["mcc TEST"]) + "\t" +  str(l_model[i]["mcc TRAIN"]) + "\t" + str(l_model[i]["acc LOO"]) + "\t" + str(l_model[i]["acc TEST"]) + "\t" +  str(l_model[i]["acc TRAIN"])+ "\t" +  str(l_model[i]["sp TEST"]) + "\t" +  str(l_model[i]["se TEST"]) +  "\t" + str(l_model[i]["mcc CV test"]) + "\n" )
                #
                path_desc = writeFiles.specificDescriptorbyData(dico_descriptor, l_model[i]["descriptors"], p_dir_model + str(i) + "_")[0]
                if type_learning == "LDA" : 
                    runOtherProg.runSaveModelLDA(path_desc)
                elif type_learning == "SVM" :
                    runOtherProg.runSaveModelSVM(path_desc)
                
                i = i + 1
        
        
    elif type (criteria_train) == list : 
        
        i = 0
        nb_model = len (l_model)
        
        if debug :print criteria_train, "criterion"
        if debug : print len (l_model) 
        
        while i < nb_model : 
            if set(criteria_train).issubset(set(l_model[i]["descriptors"])) == False :
                del l_model[i]
                nb_model = nb_model - 1
                continue
            elif l_model[i]["mcc TEST"] <= 0.77  : 
                del l_model[i]
                nb_model = nb_model - 1
                continue
            
            elif l_model[i]["mcc LOO"] <= 0.00 : 
                del l_model[i]
                nb_model = nb_model - 1
                continue
            
            
            else :
                # write file with n best models
                filout_model.write ("************************************************\n")
                filout_model.write ("----".join(l_model[i]["descriptors"]) + "\n")
                filout_model.write ("************--ACCURACY--**********\n")
                filout_model.write (str(l_model[i]["acc LOO"]) + "---" + str(l_model[i]["acc TRAIN"]) + "---" + str(l_model[i]["acc TEST"]) + "---" + str(l_model[i]["acc CV train"]) + "---" + str(l_model[i]["acc CV train SD"]) + "---" + str(l_model[i]["acc CV test"]) + "---" + str(l_model[i]["acc CV test SD"]) + "\n")
                filout_model.write ("************--SP SE--**********\n")
                filout_model.write (str(l_model[i]["sp LOO"]) + "---" + str(l_model[i]["se LOO"]) + "---" + str(l_model[i]["sp TRAIN"]) + "---" + str(l_model[i]["se TRAIN"]) + "---" + str(l_model[i]["sp TEST"]) + "---" + str(l_model[i]["se TEST"]) + "---" + str(l_model[i]["sp CV train"]) + "---" + str(l_model[i]["sp CV train SD"]) + "---" + str(l_model[i]["sp CV test"]) + "---" +  str(l_model[i]["sp CV test SD"]) + "---" + str(l_model[i]["se CV train"]) + "---" + str(l_model[i]["se CV train SD"]) + "---" + str(l_model[i]["se CV test"]) + "---" +  str(l_model[i]["se CV test SD"]) + "\n")
                filout_model.write ("************--MCC--**********\n")
                filout_model.write (str(l_model[i]["mcc LOO"]) + "---" + str(l_model[i]["mcc TRAIN"]) + "---" + str(l_model[i]["mcc TEST"]) + "---" + str(l_model[i]["mcc CV train"]) + "---" + str(l_model[i]["mcc CV train SD"]) + "---" + str(l_model[i]["mcc CV test"]) + "---" + str(l_model[i]["mcc CV test SD"]) +  "\n")
                
                filout_histo.write (str (l_model[i]["criterion mcc"]) + "\t" + str(l_model[i]["mcc TEST"]) + "\n")
                filout_score.write ("M" + str (i + 1) + "\t" + str (l_model[i]["criterion mcc"]) + "\t" + str(l_model[i]["mcc LOO"]) + "\t" + str(l_model[i]["mcc TEST"]) + "\t" +  str(l_model[i]["mcc TRAIN"]) + "\t" + str(l_model[i]["acc LOO"]) + "\t" + str(l_model[i]["acc TEST"]) + "\t" +  str(l_model[i]["acc TRAIN"])+ "\t" +  str(l_model[i]["sp TEST"]) + "\t" +  str(l_model[i]["se TEST"]) + "\t" + str(l_model[i]["mcc CV test"]) + "\n" )
                # desc
                path_desc = writeFiles.specificDescriptorbyData(dico_descriptor, l_model[i]["descriptors"], p_dir_model + str(i) + "_")[0]
                runOtherProg.runSaveModelLDA(path_desc)
                
                i = i + 1        
        
        
    if debug : print len (l_model)
    
    # summary
    filout_resume = open(p_resume,"w")
    filout_resume.write ("acc LOO: ")
    M,SD = tool.meansLModel (l_model, "acc LOO")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("acc CV train: ")
    M,SD = tool.meansLModel (l_model, "acc CV train")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("acc CV train SD: ")
    M,SD = tool.meansLModel (l_model, "acc CV train SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("acc CV test: ")
    M,SD = tool.meansLModel (l_model, "acc CV test")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("acc CV test SD: ")
    M,SD = tool.meansLModel (l_model, "acc CV test SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("acc TRAIN: ")
    M,SD = tool.meansLModel (l_model, "acc TRAIN")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("acc TEST: ")
    M,SD = tool.meansLModel (l_model, "acc TEST")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se LOO: ")
    M,SD = tool.meansLModel (l_model, "se LOO")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se CV train: ")
    M,SD = tool.meansLModel (l_model, "se CV train")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se CV train SD: ")
    M,SD = tool.meansLModel (l_model, "se CV train SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se CV test: ")
    M,SD = tool.meansLModel (l_model, "se CV test")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se CV test SD: ")
    M,SD = tool.meansLModel (l_model, "se CV test SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")   
    
    filout_resume.write ("se TRAIN: ")
    M,SD = tool.meansLModel (l_model, "se TRAIN")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se TEST: ")
    M,SD = tool.meansLModel (l_model, "se TEST")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("sp LOO: ")
    M,SD = tool.meansLModel (l_model, "sp LOO")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("sp CV train: ")
    M,SD = tool.meansLModel (l_model, "sp CV train")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("sp CV train SD: ")
    M,SD = tool.meansLModel (l_model, "sp CV train SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")    
    
    filout_resume.write ("sp CV test: ")
    M,SD = tool.meansLModel (l_model, "sp CV test")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("sp CV test SD: ")
    M,SD = tool.meansLModel (l_model, "sp CV test SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")     
    
    filout_resume.write ("sp TRAIN: ")
    M,SD = tool.meansLModel (l_model, "sp TRAIN")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("sp TEST: ")
    M,SD = tool.meansLModel (l_model, "sp TEST")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("mcc LOO: ")
    M,SD = tool.meansLModel (l_model, "mcc LOO")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("mcc CV train: ")
    M,SD = tool.meansLModel (l_model, "mcc CV train")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("mcc CV train SD: ")
    M,SD = tool.meansLModel (l_model, "mcc CV train SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")   
    
    filout_resume.write ("mcc CV test: ")
    M,SD = tool.meansLModel (l_model, "mcc CV test")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("mcc CV test SD: ")
    M,SD = tool.meansLModel (l_model, "mcc CV test SD")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")     
    
    filout_resume.write ("mcc TRAIN: ")
    M,SD = tool.meansLModel (l_model, "mcc TRAIN")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("mcc TEST: ")
    M,SD = tool.meansLModel (l_model, "mcc TEST")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    
    filout_resume.close ()
    filout_histo.close ()
    filout_model.close ()
    filout_score.close ()
    
    # control score distribution
    if type(criteria_train) == list : 
        criteria_train = 0.5 # for plot
    runOtherProg.runRscriptHisto (p_filout_histo, "MCC_criterion", brk = 10, order = criteria_train)
    matrixImageModel (l_model, p_dir + "/image_model_" + str (subname_file))
#     runOtherProg.matrixCorr (p_dir + "/image_model_" + str (subname_file), 2, debug=1)
    runOtherProg.matrixImage (p_dir + "/image_model_" + str (subname_file), 2, debug=1)
    runOtherProg.plotScoreCriterion (p_filout_score)
    
    
def matrixImageModel (l_model, p_filout):
    l_desc_head = []
    for model in l_model : 
        for desc in model ["descriptors"] : 
            if not desc in l_desc_head : 
                l_desc_head.append (desc)
                
    filout = open (p_filout, "w")
    filout.write ("\t".join (l_desc_head) + "\n")
    
    l_write = []
    i = 1
    for model in l_model :
        l_write = [] 
       
        for desc_head in l_desc_head :
            flag = 0
            for desc_model in model["descriptors"] :  
                if desc_model == desc_head : 
                    l_write.append ("1")
                    flag = 1
            if flag ==0 : 
                l_write.append ("0")
        
        filout.write ("M" + str (i) + "\t" + "\t".join (l_write) + "\n")
        i = i + 1
    
    filout.close ()
            
            
            
    
    
    
           
    

