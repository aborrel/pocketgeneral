#!/usr/bin/env Rscript


args <- commandArgs(TRUE)
file = args[1]


data = read.table(file, header = FALSE, sep = "\t")

print (data)

large = dim(data)[1]*20

if (large < 600){
	large = 600
}

#png(filename=paste(file,".png",sep = ""), width=as.integer(large), 600)
#par(mar = c(5,5,5,2))
#par(oma = c(1,1,1,1))
#barplot(data[,2], names.arg=data[,1], main="", xlab="", ylab="Number of occurencies", axes=TRUE, cex.axis = 1.2, cex.lab=1.6, cex.main=1.5, cex.names = 1.6, col = "grey", las=2, space = 0.8, cex = 1.6)
#dev.off()

svg(filename=paste(file,".svg",sep = ""),  bg = "transparent", 7,6)
par(mar = c(5,5,1,1))
barplot(as.matrix(data[2,]), names.arg=data[1,], main="", xlab="Number of Lipinski's fails", axes=TRUE, cex.axis = 1.2, cex = 1.3, space = 0.3, ylab = "Number of occurrences", cex.lab = 1.5, col = "grey")


if (max (data[2,]) > 200){
  y_grid = seq(0, max (data[2,]), 50)
}else if (max (data[,2]) > 100){
  y_grid = seq(0, max (data[2,]), 20)
}else if (max (data[,2]) > 50){
  y_grid = seq(0, max (data[2,]), 10)
}else {
  y_grid = seq(0, max (data[2,]), 5)
}


for (y in y_grid){
  segments (0, y, length(data[1,]) + 5, y, lty = 2, col = "black", lwd = 1.5)
}

dev.off()

