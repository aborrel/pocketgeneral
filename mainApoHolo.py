"""
BORREL Alexandre
09-2012
"""

import globalFonction
import runOtherProg
import writeFiles
import pathDirectory
import tool
import analysis
import superposeStructure

from os import listdir, path, remove, system
from re import search

def main (path_file_model, name_dataset="ApoForm", pocket_retrieve_type = "Fpocket"):
    
    
    dico_dataset = globalFonction.calculDatasetDictionary(name_dataset,genrate_files=0, pocket_remove=0)
    
#    print len(dico_dataset.keys())

    path_file_correspondace = writeFiles.corespondanceApoHolo (dico_dataset, pathDirectory.result(name_dataset) + "correspondencePDB")
    
    # divise dataset    
    
    dico_dataset_type_apo = tool.selectOnlyTypeStructure(dico_dataset, "apo structure")
    dico_dataset_type_holo = tool.selectOnlyTypeStructure(dico_dataset, "holo structure") 
    
    #print dico_dataset_type_holo.keys ()
    
#     print len (dico_dataset_type_apo.keys ())
#     print dico_dataset_type_holo[dico_dataset_type_holo.keys ()[1]]
        
    
    # accessibility solvent
#     runOtherProg.globalNACCESS(dico_dataset_type_apo, name_dataset)
#     runOtherProg.globalNACCESS(dico_dataset_type_holo, name_dataset)
    
    # superimpose -> retrieve matrix transloc
#     superposeStructure.superposeApoHolo(dico_dataset, name_dataset, pocket_retrieve_type)
    
    ######################
    # Pocket Estimation  #
    ######################    
    
    # pocket estimation holo
    globalFonction.pocketEstimation(name_dataset, dico_dataset_type_holo, "proximity", runFpocket = 0)
    globalFonction.pocketEstimation(name_dataset, dico_dataset_type_holo, pocket_retrieve_type, runFpocket = 0)
    
    # pocket estimation apo
    globalFonction.pocketEstimationApoForm(name_dataset, dico_dataset_type_apo, dico_dataset_type_holo, pocket_retrieve_type, runFpocket = 0)
    
    
    ##############
    # Descriptor #
    ##############
    
    # descriptor
    dico_descriptors_type_apo = globalFonction.retrieveGlobalDescriptors (pocket_retrieve_type, "none", dico_dataset_type_apo, name_dataset, write_file = 0, calcul_descriptor = 1, option_separate = 1)
    dico_descriptors_type_holo = globalFonction.retrieveGlobalDescriptors (pocket_retrieve_type, "none", dico_dataset_type_holo, name_dataset, write_file = 0, calcul_descriptor = 1, option_separate = 1)

    #####################
    # write data global #
    #####################
    path_file_descriptor_apo = writeFiles.globalDescriptors(dico_descriptors_type_apo, pathDirectory.result(name_dataset + "/" + pocket_retrieve_type) + "apo_all_pocket.data")
    path_file_descriptor_holo = writeFiles.globalDescriptors(dico_descriptors_type_holo, pathDirectory.result(name_dataset+ "/" + pocket_retrieve_type) + "holo_all_pocket.data")
  
    path_file_RMSD = pathDirectory.searchRMSDFile (pathDirectory.descriptor(name_dataset))
    
    path_dir_result = pathDirectory.result(name_dataset + "/" + pocket_retrieve_type)
    # color for ACP

    writeFiles.colorACPFile (dico_descriptors_type_apo)
       
    ###############
    #     ACP     #
    ###############
###    # apo protein
    analysis.specificACP("global", dico_descriptors_type_apo, path_dir_result + "apo_globalData", mainACP = "All_descriptors_Apo")
   
###    # holo protein
#     analysis.specificACP("global", dico_descriptors_type_holo, path_dir_result + "holo_globalData", mainACP = "All_descriptors_holo")
    # analysis.specificACP(["hydrophobic_kyte","p_Ooh_atom", "p_aromatic_residues"], dico_descriptors_type_holo, path_dir_result + "holo_descModel", mainACP = "Specific_descriptors")
   
###    # PCA apo and holo same plot
#     runOtherProg.ACPDataset (path_file_descriptor_apo, path_file_descriptor_holo, path_dir_result + "PCA_apo_holo")
#     analysis.ACPTwoDatasetDescriptor(dico_descriptors_type_apo, dico_descriptors_type_holo, ["hydrophobic_kyte","p_Ooh_atom", "p_aromatic_residues"], path_dir_result + "desc_model", correspondance_file=path_file_correspondace)
#     analysis.ACPTwoDatasetDescriptor(dico_descriptors_type_apo, dico_descriptors_type_holo, "radi", path_dir_result+ "radi", correspondance_file=path_file_correspondace)
#     analysis.ACPTwoDatasetDescriptor(dico_descriptors_type_apo, dico_descriptors_type_holo,["RADIUS_HULL", "DIAMETER_HULL", "SURFACE_HULL", "VOLUME_HULL", "SMALLEST_SIZE", "INERTIA_3", "INERTIA_1", "FACE", "PCI", "PSI", "RADIUS_CYLINDER", "X._ATOM_CONVEXE", "CONVEX.SHAPE_COEFFICIENT", "INERTIA_2", "C_RESIDUES", "C_ATOM"], path_dir_result+ "geoOnly", correspondance_file=path_file_correspondace)
#      
    
    #######################
    # correlation pocket  #
    #######################   
    # -radar plot-
    analysis.correlationDescApoHolo (path_file_descriptor_apo, path_file_descriptor_holo, path_file_correspondace)
    
    
    
    ################
    # RMSD pocket  #
    ################
    
    #analysis.RMSDPockets (dico_dataset, pathDirectory.result(name_dataset + "/RMSD"), name_dataset)
    
   
    ########################
    # Histogram descriptor #
    ########################
#     analysis.histogramFonctionRMSD (dico_descriptors_type_apo, dico_descriptors_type_holo, path_file_RMSD, path_dir_result, ["hydrophobic_kyte"])
#     analysis.histogramFonctionRMSD (dico_descriptors_type_apo, dico_descriptors_type_holo, path_file_RMSD, path_dir_result, ["p_Ooh_atom"])
#     analysis.histogramFonctionRMSD (dico_descriptors_type_apo, dico_descriptors_type_holo, path_file_RMSD, path_dir_result, ["p_aromatic_residues"])
#     
   
    ###################
    # apply LDA model #
    ###################
#    # global
#    # apo
     
#     globalFonction.applyModel(path_file_model, path_file_descriptor_apo, path_dir_result + "predictApo.result") # laisse seulement une commande dans la fonction pour apres car surement ACP ou autre
#    
#    # holo
#     globalFonction.applyModel(path_file_model, path_file_descriptor_holo, path_dir_result + "predictHolo.result")
    
 
 
 
    
    
def multiModelTestApo (path_dir_model, name_dir = "", name_dataset="ApoForm", pocket_retrieve_type = "Fpocket"): 
    
#     f = 0
    dico_dataset = globalFonction.calculDatasetDictionary(name_dataset,0)
    
    path_file_correspondace = writeFiles.corespondanceApoHolo (dico_dataset, pathDirectory.result(name_dataset) + "correspondencePDB")
    
    # divise dataset    
    dico_dataset_type_apo = tool.selectOnlyTypeStructure(dico_dataset, "apo structure")
    dico_dataset_type_holo = tool.selectOnlyTypeStructure(dico_dataset, "holo structure") 
    
    # dictionnary with descriptors   
    dico_descriptors_type_apo = globalFonction.retrieveGlobalDescriptors (pocket_retrieve_type, "none", dico_dataset_type_apo, name_dataset, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_descriptors_type_holo = globalFonction.retrieveGlobalDescriptors (pocket_retrieve_type, "none", dico_dataset_type_holo, name_dataset, write_file = 0, calcul_descriptor = 0, option_separate = 1)

    # folder result
    path_dir_result = pathDirectory.result(name_dataset + "/" + name_dir)
    if not path.exists(path_dir_result) : 
        pathDirectory.generatePath(path_dir_result)
    else : 
        system( "rm -f " + path_dir_result + "*" )
    #####################
    # write data global #
    #####################
    path_file_descriptor_apo = writeFiles.globalDescriptors(dico_descriptors_type_apo, pathDirectory.result(name_dataset) + "apo_all_pocket.data")
    path_file_descriptor_holo = writeFiles.globalDescriptors(dico_descriptors_type_holo, pathDirectory.result(name_dataset) + "holo_all_pocket.data")
  

    l_file_model = listdir(path_dir_model)
    p_file_result_apo = path_dir_result + "bestApo.result"
    p_file_result_holo = path_dir_result + "bestHolo.result"
    # check exist ?
    if path.exists(p_file_result_apo) : 
        remove(p_file_result_apo)
    
    l_proba_apo = []
    l_proba_holo = []
    for p_file_model in l_file_model : 
        if search("Rdata", p_file_model) : 
            n_model = p_file_model.split ("_")[0]
            out_apo = runOtherProg.predictLDA (path_dir_model + p_file_model, path_file_descriptor_apo, p_file_result_apo, p_out= path_dir_result + n_model + "_" +  path.basename(path_file_descriptor_apo))
            out_holo = runOtherProg.predictLDA (path_dir_model + p_file_model, path_file_descriptor_holo, p_file_result_holo, p_out= path_dir_result + n_model + "_" +  path.basename(path_file_descriptor_holo))
            
            l_proba_apo.append (out_apo[0])
            l_proba_holo.append (out_holo[0])
            
    # average model apply
    globalFonction.bestmultiModelApo (p_file_result_apo)
    globalFonction.bestmultiModelApo (p_file_result_holo)

    # average probability
    globalFonction.averageScoreDruggProba (l_proba_apo, path_dir_result + "resume_apo.Proba")
    globalFonction.averageScoreDruggProba (l_proba_holo, path_dir_result + "resume_holo.Proba")


# Dataset Apo Schmitke global -> refaire la fonction de codage  #
#################################################################

# main ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/AutoSelected/autoselected.Rdata", name_dataset="ApoForm138", pocket_retrieve_type = "DogSite")
# main ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/AutoSelected/autoselected.Rdata", name_dataset="ApoForm138")



######################################################
# Dataset Apo Schmitke clean -> only one apo by holo #
#                   APO CLEAN                        #
######################################################

# main ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/AutoSelected/autoselected.Rdata", name_dataset="ApoFormClean")
# main ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/AutoSelected/autoselected.Rdata", name_dataset="ApoFormClean", pocket_retrieve_type = "DogSite")

##############
# apo huang  #
##############

# main ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/AutoSelected/autoselected.Rdata", name_dataset = "ApoHuang")


##############################################
#  test best models proposed with selection  #
##############################################


#################
#  Best model   #
#################

############
############
# APO 138  #
############
############

# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/", name_dataset="ApoForm138", name_dir = "PPE_3desc_4bestModel-fpocket", pocket_retrieve_type = "Fpocket")
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/", name_dataset="ApoForm138", name_dir = "PPE_3desc_4bestModel-Dog", pocket_retrieve_type = "DogSite")
# # # # # 
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels/", name_dataset="ApoForm138", name_dir = "PPE_4desc_4bestModel-fpocket", pocket_retrieve_type = "Fpocket")
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels/", name_dataset="ApoForm138", name_dir = "PPE_4desc_4bestModel-Dog", pocket_retrieve_type = "DogSite")
# # # # # 
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_3/BestModels/", name_dataset="ApoForm138", name_dir = "OPE_3desc_4bestModel-fpocket", pocket_retrieve_type = "Fpocket")
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_3/BestModels/", name_dataset="ApoForm138", name_dir = "OPE_3desc_4bestModel-Dog", pocket_retrieve_type = "DogSite")
# # # # # 
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/", name_dataset="ApoForm138", name_dir = "OPE_4desc_4bestModel-fpocket", pocket_retrieve_type = "Fpocket")
# # # # # multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/", name_dataset="ApoForm138", name_dir = "OPE_4desc_4bestModel-Dog", pocket_retrieve_type = "DogSite")


# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/", name_dataset="ApoForm138", name_dir = "OPE-prox4-fpocket-21-11-2014", pocket_retrieve_type = "Fpocket")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/", name_dataset="ApoForm138", name_dir = "OPE-prox4-Dog-21-11-2014", pocket_retrieve_type = "DogSite")
# 
multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/", name_dataset="ApoForm138", name_dir = "PockDrug-fpocket-fpocket-12-01-2014", pocket_retrieve_type = "Fpocket")
multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/", name_dataset="ApoForm138", name_dir = "PockDrug-fpocket-Dog-12-01-2014", pocket_retrieve_type = "DogSite")

# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/prox6/LDA/selectedDesc_4/BestModels-5/", name_dataset="ApoForm138", name_dir = "OPE-prox6-fpocket-08-12-2014-5", pocket_retrieve_type = "Fpocket")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/prox6/LDA/selectedDesc_4/BestModels-5/", name_dataset="ApoForm138", name_dir = "OPE-prox6-Dog-08-12-2014-5", pocket_retrieve_type = "DogSite")


# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/prox6/LDA/selectedDesc_4/BestModels-6/", name_dataset="ApoForm138", name_dir = "OPE-prox6-fpocket-09-12-2014-6", pocket_retrieve_type = "Fpocket")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/prox6/LDA/selectedDesc_4/BestModels-6/", name_dataset="ApoForm138", name_dir = "OPE-prox6-Dog-08-12-2014-6", pocket_retrieve_type = "DogSite")




# test global
#multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels0.64_0.64/", name_dataset="ApoForm138", name_dir = "PPE_3desc_global0.64-fpocket", pocket_retrieve_type = "Fpocket")


# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-2/", name_dataset="ApoForm138", name_dir = "OPE-prox5.5-fpocket-11-12-2014-2", pocket_retrieve_type = "Fpocket")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-2/", name_dataset="ApoForm138", name_dir = "OPE-prox5.5-Dog-11-12-2014-2", pocket_retrieve_type = "DogSite")



##############
##############
# APO clean  #
##############
##############

# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels0.72/", name_dataset="ApoFormClean", name_file_result = "POE3_bestDog-7Model", pocket_retrieve_type = "DogSite")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels0.72/", name_dataset="ApoFormClean", name_file_result = "POE3_bestFpocket-7Model")
# 
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels/", name_dataset="ApoFormClean", name_file_result = "POE3_bestDog-5Model", pocket_retrieve_type = "DogSite")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels/", name_dataset="ApoFormClean", name_file_result = "POE3_bestFpocket-5Model")

# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels/", name_dataset="ApoFormClean", name_file_result = "POE3_bestDog-BestHKModel", pocket_retrieve_type = "DogSite")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels/", name_dataset="ApoFormClean", name_file_result = "POE3_bestFpocket-BestHKModel")

# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels/", name_dataset="ApoFormClean", name_file_result = "POE3_bestDog-BestHKModel", pocket_retrieve_type = "DogSite")
# multiModelTestApo ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc/BestModels/", name_dataset="ApoFormClean", name_file_result = "POE3_bestFpocket-BestHKModel")



