from os import listdir, remove
from re import search

import runOtherProg
import getResidues
import overlapPocket





def mainOverlap (p_dir_pocketEst1, p_dir_pocketEst2, p_dir_pro, p_result):
    
    
    
    # run NACCESS with complexe and pockets
    l_p_complexe = listdir(p_dir_pro)
    for file_pro in l_p_complexe : 
        if not search (".pdb", file_pro) : 
            continue
        # PDB ID
        PDB_ID = file_pro[0:4]
        
        # open file overlapping
        filout_RO = open (p_result + file_pro[0:-4] + "_RO.txt", "w")
        filout_SO = open (p_result + file_pro[0:-4] + "_SO.txt", "w")
        filout_MO = open (p_result + file_pro[0:-4] + "_MO.txt", "w")
        
        
        p_pro = p_dir_pro + file_pro
        print p_pro
        
        # NACESS -> on complexe
        p_pro_naccess = runOtherProg.runNACESS(p_pro)
        
        # pocke est1
        l_p_pock1 = listdir(p_dir_pocketEst1)
        l_p_pock2 = listdir(p_dir_pocketEst2)
        
        # poke est2
        l_temp_file_pock1 = []
        for file_pock1 in l_p_pock1 :
            if search (PDB_ID, file_pock1) and search (".pdb", file_pock1): 
                p_files_pocket1_ACC  =  getResidues.getAccAtom(p_pro_naccess[0], p_dir_pocketEst1 + file_pock1)
                remove(p_files_pocket1_ACC[-1])
                l_temp_file_pock1.append (p_dir_pocketEst1 + file_pock1) 
            
        # pocke est2
        l_temp_file_pock2 = []
        for file_pock2 in l_p_pock2 : 
            if search (PDB_ID, file_pock2) and search (".pdb", file_pock2): 
                p_files_pocket2_ACC  =  getResidues.getAccAtom(p_pro_naccess[0], p_dir_pocketEst2 + file_pock2)
                remove(p_files_pocket2_ACC[-1])
                l_temp_file_pock2.append (p_dir_pocketEst2 + file_pock2) 
        
        
        
        # overlapping pocket
        #l_MO = []
        #l_RO = []
        #l_SO = []
        #flag_header_RO = 0
        
        # header file
        #if flag_header_RO == 0 :
        header = []
        for name_file_pock2 in l_temp_file_pock2 :
            header.append (name_file_pock2.split ("/")[-1][0:-4]) 

        filout_SO.write ("\t".join(header) + "\n")
        filout_RO.write ("\t".join(header) + "\n")    
        filout_MO.write ("\t".join(header) + "\n")
        
        
        for p_pocket_est1 in l_temp_file_pock1 : 
            l_MO = []
            l_SO = []
            l_RO = []
            for p_pocket_est2 in l_temp_file_pock2 :
                
                print "###### DEBUG ########"
                print PDB_ID
                print p_pocket_est1
                print p_pocket_est2
                print p_pocket_est1[0:-4] + "_ACC.asa"
                print p_pocket_est2[0:-4] + "_ACC.asa"
                
                
                
                l_SO.append(str(overlapPocket.scoreOverlap(p_pocket_est1, p_pocket_est2)))
                l_MO.append(str(overlapPocket.MO(p_pocket_est1, p_pocket_est2, p_pocket_est1[0:-4] + "_ACC.asa", p_pocket_est2[0:-4] + "_ACC.asa")))
                l_RO.append (str(overlapPocket.MO(p_pocket_est2, p_pocket_est1, p_pocket_est2[0:-4] + "_ACC.asa", p_pocket_est1[0:-4] + "_ACC.asa")))
        
            filout_SO.write (p_pocket_est1.split ("/")[-1][0:-4] + "\t" + "\t".join(l_SO) + "\n")
            filout_MO.write (p_pocket_est1.split ("/")[-1][0:-4] + "\t" + "\t".join(l_MO) + "\n")
            print l_RO, "L_RO"
            print l_MO, "L_MO"
            print l_SO, "L_SO"
            filout_RO.write (p_pocket_est1.split ("/")[-1][0:-4] + "\t" + "\t".join(l_RO) + "\n")        
                
    
        filout_SO.close ()
        filout_MO.close ()
        filout_RO.close ()






# ----RUN----#
##############

mainOverlap ("/home/borrel/poche_lelieEstimator/fpocket_sel/", "/home/borrel/poche_lelieEstimator/prox_sel/", "/home/borrel/poche_lelieEstimator/structures/", "/home/borrel/poche_lelieEstimator/R_fpocket_prox/")
mainOverlap ("/home/borrel/poche_lelieEstimator/fpocket_sel/", "/home/borrel/poche_lelieEstimator/SLE_pocket/", "/home/borrel/poche_lelieEstimator/structures/", "/home/borrel/poche_lelieEstimator/R_fpocket_SLE/")
mainOverlap ("/home/borrel/poche_lelieEstimator/prox_sel/", "/home/borrel/poche_lelieEstimator/SLE_pocket/", "/home/borrel/poche_lelieEstimator/structures/", "/home/borrel/poche_lelieEstimator/R_prox_SLE/")








