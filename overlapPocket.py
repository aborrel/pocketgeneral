import parsePDB
import parseNACCESS
import superposeStructure
import writePDBfile
import globalFonction
import pathDirectory

import os
import numpy 
import checkPocket
import runOtherProg



def scoreOverlap (path_file_pocket1, path_file_pocket2):
    
    list_atom_pocket1 = parsePDB.loadCoordSectionPDB(path_file_pocket1)
    list_atom_pocket2 = parsePDB.loadCoordSectionPDB(path_file_pocket2)
    
    int_nb_atom = len (list_atom_pocket1) + len (list_atom_pocket2)
    
    
    int_commun = 0.0
    for dico_atom_pocket1 in list_atom_pocket1 : 
        for dico_atom_pocket2 in list_atom_pocket2 : 
            if dico_atom_pocket1["x"] == dico_atom_pocket2["x"] and dico_atom_pocket1["y"] == dico_atom_pocket2["y"] and dico_atom_pocket1["z"] == dico_atom_pocket2["z"] : 
                int_commun  = int_commun + 1
                break
    
    if (int_nb_atom - int_commun) == 0.0 : 
        return 0.0
    else : 
        return int_commun / (int_nb_atom - int_commun)
#     return int_commun


def communAtom(path_file_pocket1, path_file_pocket2) : 
    
    list_atom_pocket1 = parsePDB.loadCoordSectionPDB(path_file_pocket1)
    list_atom_pocket2 = parsePDB.loadCoordSectionPDB(path_file_pocket2)
    
    int_nb_atom = len (list_atom_pocket1) + len (list_atom_pocket2)
    
    
    int_commun = 0.0
    for dico_atom_pocket1 in list_atom_pocket1 : 
        for dico_atom_pocket2 in list_atom_pocket2 : 
            if dico_atom_pocket1["x"] == dico_atom_pocket2["x"] and dico_atom_pocket1["y"] == dico_atom_pocket2["y"] and dico_atom_pocket1["z"] == dico_atom_pocket2["z"] : 
                int_commun  = int_commun + 1
                break
    
    return int_commun , len (list_atom_pocket1), len (list_atom_pocket2)


def MO (path_file_pocket1, path_file_pocket2, path_file_pocket1_asa, path_file_pocket2_asa, debug = 1):
    
    
    if debug : 
        print "***************"
        print path_file_pocket1
        print path_file_pocket2
        print path_file_pocket1_asa
        print path_file_pocket2_asa
        print os.path.split(path_file_pocket1)
        print "***************"
    
    
    l_atom_pocket1 = parsePDB.loadCoordSectionPDB(path_file_pocket1)
    l_atom_pocket2 = parsePDB.loadCoordSectionPDB(path_file_pocket2) 
    
    l_atom_asa_pocket1 = parseNACCESS.fileASA(path_file_pocket1_asa)
    l_atom_asa_pocket2 = parseNACCESS.fileASA(path_file_pocket2_asa)    
    
    l_commun1 = []
    for dico_atom_pocket1 in l_atom_pocket1 : 
        for dico_atom_pocket2 in l_atom_pocket2 : 
            if dico_atom_pocket1["x"] == dico_atom_pocket2["x"] and dico_atom_pocket1["y"] == dico_atom_pocket2["y"] and dico_atom_pocket1["z"] == dico_atom_pocket2["z"] : 
                l_commun1.append (dico_atom_pocket1)
#                 l_commun2.append (dico_atom_pocket2)
                break
   
    score_g = 0.0
    score_inter = 0.0
    for dico_atom_asa in l_atom_asa_pocket1 : 
        score_g = score_g + dico_atom_asa["ABS"]
        for commun_atom in l_commun1 :
            if  dico_atom_asa["atomSeq"] == commun_atom["serial"] : 
                score_inter = score_inter + dico_atom_asa["ABS"]
                # break in case of same number residues
                break
    
    
    if debug :
        print len (l_commun1)
        print score_g
        print score_inter
        try : print score_inter / score_g
        except : pass
    
    
    if score_g == 0.0 : 
        return 0.0
    return score_inter / score_g
    

def scoreOverlapRealtive (path_file_pocket1, path_file_pocket2):
    
    list_atom_pocket1 = parsePDB.loadCoordSectionPDB(path_file_pocket1)
    list_atom_pocket2 = parsePDB.loadCoordSectionPDB(path_file_pocket2)
    
    int_commun = 0.0
    for dico_atom_pocket1 in list_atom_pocket1 : 
        for dico_atom_pocket2 in list_atom_pocket2 : 
            if dico_atom_pocket1["x"] == dico_atom_pocket2["x"] and dico_atom_pocket1["y"] == dico_atom_pocket2["y"] and dico_atom_pocket1["z"] == dico_atom_pocket2["z"] : 
                int_commun  = int_commun + 1
                break
    
    if len (list_atom_pocket1) == 0.0 : 
        return 0.0
    else : 
        return int_commun / len (list_atom_pocket1)



def scoreOverlapAcc (path_file_pocket1, path_file_pocket2, path_file_pocket1_asa, path_file_pocket2_asa, debug = 0):
    
    
    if debug : 
        print "***************"
        print path_file_pocket1
        print path_file_pocket2
        print path_file_pocket1_asa
        print path_file_pocket2_asa
        print os.path.split(path_file_pocket1)
        print "***************"
    
    
    l_atom_pocket1 = parsePDB.loadCoordSectionPDB(path_file_pocket1)
    l_atom_pocket2 = parsePDB.loadCoordSectionPDB(path_file_pocket2) 
    
    l_atom_asa_pocket1 = parseNACCESS.fileASA(path_file_pocket1_asa)
    l_atom_asa_pocket2 = parseNACCESS.fileASA(path_file_pocket2_asa)    
    
    l_commun1 = []
    for dico_atom_pocket1 in l_atom_pocket1 : 
        for dico_atom_pocket2 in l_atom_pocket2 : 
            if dico_atom_pocket1["x"] == dico_atom_pocket2["x"] and dico_atom_pocket1["y"] == dico_atom_pocket2["y"] and dico_atom_pocket1["z"] == dico_atom_pocket2["z"] : 
                l_commun1.append (dico_atom_pocket1)
#                 l_commun2.append (dico_atom_pocket2)
                break
   
    score_g = 0.0
    score_inter = 0.0
    for dico_atom_asa1 in l_atom_asa_pocket1 : 
        score_g = score_g + dico_atom_asa1["ABS"]
        for commun_atom in l_commun1 :
            if  dico_atom_asa1["atomSeq"] == commun_atom["serial"] : 
                score_inter = score_inter + dico_atom_asa1["ABS"]
                # break in case of same number residues
                break
    
    for dico_atom_asa2 in l_atom_asa_pocket2 : 
        score_g = score_g + dico_atom_asa2["ABS"]
    
    
    for dico_atom_asa1 in l_atom_asa_pocket1 : 
        score_g = score_g + dico_atom_asa1["ABS"]
        for commun_atom in l_commun1 :
            if  dico_atom_asa1["atomSeq"] == commun_atom["serial"] : 
                score_inter = score_inter + dico_atom_asa1["ABS"]
                # break in case of same number residues
                break
    
    
    if debug :
        print len (l_commun1)
        print score_g
        print score_inter
        try : print score_inter / score_g
        except : pass
    
    
    if score_g == 0.0 : 
        return 0.0
    return score_inter / score_g



def overlapAnalysis (l_dist, name_dataset, select_pocket_based = "" ):
    """
    Selection pocket function of pocket seleced manualy
    """
    
    
    
    pr_result = pathDirectory.result("overlapThresold")
    filout = open (pr_result + "scoreOverlap", "w")
    filout.write ("Thresold\tSOfpocket\tSDfpocket\tSODog\tSDDog\n")
    
    dico_dataset = globalFonction.calculDatasetDictionary(name_dataset, pocket_remove=0, genrate_files=0)
    
    for dist in l_dist : 
        #print dist
        pocket_retrive_type = "prox" + str (dist)
        globalFonction.pocketEstimation(name_dataset, dico_dataset, pocket_retrive_type)
        
        if select_pocket_based != "" : 
            checkPocket.cleanPocketSetVSDataset (name_dataset, pocket_retrive_type, select_pocket_based)
        
        l_SO_fpocket = globalFonction.SODataset(name_dataset, "Fpocket", pocket_retrive_type)
        l_SO_dog = globalFonction.SODataset(name_dataset, "DogSite", pocket_retrive_type)
        
        mean_SO_fpocket = numpy.mean(l_SO_fpocket)
        sd_SO_fpocket = numpy.std(l_SO_fpocket)
        mean_SO_dog = numpy.mean(l_SO_dog)
        sd_SO_dog = numpy.std(l_SO_dog)
        filout.write (str (dist) + "\t" + str (mean_SO_fpocket) + "\t" + str (sd_SO_fpocket) + "\t" + str (mean_SO_dog) + "\t" + str (sd_SO_dog) + "\n")
    filout.close ()
    
    runOtherProg.overlapVSTresold(pr_result + "scoreOverlap")






#MO ("/home/borrel/poche_lelieEstimator/SLE_pocket/1IVD_pocket11.pdb", "/home/borrel/poche_lelieEstimator/prox_sel/1IVD_pocket-NAG_472_NAG_473_A-NAG_484_NAG_485_A_atm.pdb","/home/borrel/poche_lelieEstimator/SLE_pocket/1IVD_pocket11_ACC.asa","/home/borrel/poche_lelieEstimator/prox_sel/1IVD_pocket-NAG_472_NAG_473_A-NAG_484_NAG_485_A_atm_ACC.asa")
     
#MO ("/home/borrel/poche_lelieEstimator/prox_sel/1IVD_pocket-NAG_472_NAG_473_A-NAG_484_NAG_485_A_atm.pdb", "/home/borrel/poche_lelieEstimator/SLE_pocket/1IVD_pocket11.pdb", "/home/borrel/poche_lelieEstimator/prox_sel/1IVD_pocket-NAG_472_NAG_473_A-NAG_484_NAG_485_A_atm_ACC.asa","/home/borrel/poche_lelieEstimator/SLE_pocket/1IVD_pocket11_ACC.asa")
  



   

