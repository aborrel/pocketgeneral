from re import search, sub
import tool


def retrieveAllAccuracy (path_filin):
    """
    Retrieve accuracy one global LDA
    args: -> path filin
    return: -> accuracy train
            -> accuracy test
            -> number of descriptors
    """
    
    filin = open (path_filin, "r")
    list_lines = filin.readlines ()
    filin.close ()
    nb_descriptor = list_lines[2].split (" ")[1].strip().replace ("\"", "")
    list_accuracy = []
    for line_file in list_lines : 
        if search("accuracy", line_file) : 
            list_accuracy.append (line_file.split (":")[1].strip().replace ("\"", ""))
    
    return nb_descriptor, list_accuracy[0], list_accuracy[1], list_accuracy[2]


    
def retrieveQualityLOO (path_filin):
    """
    Retrieve quality prediction in leave one out
    args: -> path filin
    return: -> accuracy 
            -> Recall
            -> precision
            -> sensibility
            -> specificity
    """
    filin = open (path_filin, "r")
    list_lines = filin.readlines ()
    filin.close ()
    
    nb_descriptor = list_lines[2].split (" ")[1].strip().replace ("\"", "")
    for line_file in list_lines : 
        if search("accuracy", line_file) : 
            accuracy = line_file.split (" : ")[1].strip().replace ("\"", "")
        elif search ("precision", line_file) : 
            precision = line_file.split (" : ")[1].strip().replace ("\"", "")
        elif search ("recall", line_file) : 
            recall = line_file.split (" : ")[1].strip().replace ("\"", "")
        elif search ("sensibility", line_file): 
            sensibility = line_file.split (" : ")[1].strip().replace ("\"", "")
        elif search ("specificity", line_file):
            specificity = line_file.split (" : ")[1].strip().replace ("\"", "")
    
    return nb_descriptor, accuracy, precision, recall, sensibility, specificity
 
 
def retrieveDescriptorAccModel (list_lines) : 

    nb_lines = len (list_lines)

    i=0
    while i< nb_lines : 
        if search ("\[1\] \"descriptor\"", list_lines[i]) :
            list_descriptor = tool.formatLinesDescriptor (list_lines[i + 1])
            if not search ("^\[1\]",list_lines[i+2]) : 
                list_descriptor = list_descriptor + tool.formatLinesDescriptor(list_lines[i+2])
                if not search ("^\[1\]",list_lines[i+3]) :
                    list_descriptor = list_descriptor + tool.formatLinesDescriptor(list_lines[i+3])
                    l_des_implication = sub("[ ]{2,}", " ",list_lines[i+4].strip())
                    l_des_implication = l_des_implication.split (" ")[1:]
                    acc_global = tool.formatLinesAcc(list_lines[i+6])
                else : 
                    l_des_implication = sub("[ ]{2,}", " ",list_lines[i+3].strip())
                    l_des_implication = l_des_implication.split (" ")[1:]
                    acc_global = tool.formatLinesAcc(list_lines[i+5])
            else :
                acc_global = tool.formatLinesAcc(list_lines[i+4])
                
                l_des_implication = sub("[ ]{2,}", " ",list_lines[i+2].strip())
                l_des_implication = l_des_implication.split (" ")[1:]
            
            mcc_global = tool.formatLinesAcc(list_lines[-2])
            spse_global = tool.formatLinesAcc(list_lines[-4])
            return [list_descriptor, acc_global, mcc_global, spse_global, l_des_implication]
        i = i + 1
            
    

    
 
def retrieveQualityLDA (l_lines):
    """
    If classic format out LDA
    args: -> path filin
    return: -> accuracy 
            -> Recall
            -> precision
            -> sensibility
            -> specificity
    """
    d_out = {}
    
    for l in l_lines : 
        if search("accuracy", l) : 
            d_out["acc"] = l.split (" : ")[1].strip().replace ("\"", "")
        elif search ("precision", l) : 
            d_out["precision"] = l.split (" : ")[1].strip().replace ("\"", "")
        elif search ("sensibility", l): 
            d_out["se"] = l.split (" : ")[1].strip().replace ("\"", "")
        elif search ("specificity", l):
            d_out["sp"] = l.split (" : ")[1].strip().replace ("\"", "")
        elif search ("MCC", l):
            d_out["mcc"] = l.split (" : ")[1].strip().replace ("\"", "")
            if d_out["mcc"] == "NaN" : 
                d_out["mcc"] = 0.0
            
    print d_out    
    return d_out 
