"""
BORREL Alexandre
04-2012
"""
# personal modules
import analysis
import pathDirectory
import downloadFile
import tool
import checkPocket
import dataSet
import writeFiles
import preparePDB
import identityCalcul
import runOtherProg
import getResidues
import descriptor
import loadDescriptors
import parameters
import superposeStructure
import overlapPocket
import rateQuality

# general module
import os, re, numpy
import descriptorEnergy
import parseOutLDA


def analysisGlobalDescriptor (dictionary_descriptor, dataset_name, pocket_type, ttest = 1, ACP = 1, LDAGlobal = 1, CART = 1, SVM = 1,  correlation = 1, histogram = 1, selection_variable = 0, radomForest = 1, regLog = 1, criteria_train=100, criteria_loo = 0, criteria_CV = 0, criteria_test =0, nb_desc_model = 3):
    
    path_file_color = writeFiles.colorACPFile (dictionary_descriptor) # color for ACP
    
    list_with_ligand = ["p_hydrophobic_residues", "PCI", "SURFACE_HULL", "p_Ntrp_atom"]
    list_without_ligand = ["hydrophobic_kyte","DIAMETER_HULL", "p_aromatic_residues"]
    l_arom = [ "p_aromatic_residues", "p_Car_atom","p_Otyr_atom", "p_Ntrp_atom", "p_ND1_atom", "p_NE2_atom"]
    
    l_M1_prox = ["SMALLEST_SIZE", "SURFACE_HULL","PCI"]
    l_M2_prox = ["SURFACE_HULL", "PCI","p_Ntrp_atom" ]
    l_M3_prox = ["p_polar_residues", "p_Nlys_atom", "p_side_chain_atom" ]
    l_M4_prox = ["p_polar_residues", "p_Nlys_atom", "p_main_chain_atom" ]
    l_M5_prox = ["p_polar_residues", "p_oxygen_atom", "p_side_chain_atom" ]
    l_M6_prox = ["p_polar_residues", "p_Nlys_atom", "p_main_chain_atom" ]
    
    l_M7_prox = ["SURFACE_HULL", "PCI","hydrophobic_kyte" ]
    l_M8_prox = ["p_polar_residues", "p_Nlys_atom", "hydrophobic_kyte" ]
    l_M10_prox = ["p_polar_residues", "p_oxygen_atom", "hydrophobic_kyte" ]
    l_M11_prox = ["p_polar_residues", "hydrophobic_kyte", "p_main_chain_atom" ]
    
    
    # cross analysis
    
    l_M1_test = ["SURFACE_HULL", "hydrophobic_kyte"]
    l_M2_test = ["SMALLEST_SIZE", "hydrophobic_kyte" ]
    l_M3_test = ["RADIUS_HULL", "hydrophobic_kyte"]
    l_M4_test = ["DIAMETER_HULL", "hydrophobic_kyte"]
    l_M5_test = ["RADIUS_CYLINDER", "hydrophobic_kyte"]
    l_M6_test = ["VOLUME_HULL", "hydrophobic_kyte"]
    
    l_M7_test = ["SURFACE_HULL", "p_Otyr_atom"]
    l_M8_test = ["SMALLEST_SIZE", "p_Otyr_atom" ]
    l_M9_test = ["RADIUS_HULL", "p_Otyr_atom"]
    l_M10_test = ["DIAMETER_HULL", "p_Otyr_atom"]
    l_M11_test = ["RADIUS_CYLINDER", "p_Otyr_atom" ]
    l_M12_test = ["VOLUME_HULL", "p_Otyr_atom"]
    
    l_M13_test = ["SURFACE_HULL", "p_aromatic_residues"]
    l_M14_test = ["SMALLEST_SIZE", "p_aromatic_residues" ]
    l_M15_test = ["RADIUS_HULL", "p_aromatic_residues"]
    l_M16_test = ["DIAMETER_HULL", "p_aromatic_residues"]
    l_M17_test = ["RADIUS_CYLINDER", "p_aromatic_residues" ]
    l_M18_test = ["VOLUME_HULL", "p_aromatic_residues"]
    
    l_M19_test = ["hydrophobic_kyte", "p_Otyr_atom"]
    l_M20_test = ["hydrophobic_kyte", "p_aromatic_residues" ]
    l_M21_test = ["p_aromatic_residues", "p_Otyr_atom"]
    
    
#     
#     l_M13_test = ["hydrophobic_kyte", "p_Otyr_atom", "SURFACE_HULL"]
#     l_M14_test = ["hydrophobic_kyte", "p_aromatic_residues", "SMALLEST_SIZE"]
#     l_M15_test = ["hydrophobic_kyte", "p_Otyr_atom", "RADIUS_HULL"]
#     
    
    l_consensus = ["DIAMETER_HULL", "hydrophobic_kyte", "p_aromatic_residues", "VOLUME_HULL", "SURFACE_HULL", "p_Otyr_atom"]    
    
    if ttest : 
        path_result = pathDirectory.result (dataset_name + "/" + pocket_type + "/Ttest")
        if len (dictionary_descriptor["Druggable"].keys()) > 1 and len (dictionary_descriptor["No-Druggable"].keys()) > 1 :  
            dico_ttest = analysis.ttest(dictionary_descriptor["Druggable"], dictionary_descriptor["No-Druggable"], path_result + "ttest_" + dataset_name )
            list_result_ttest_signif = analysis.retrieveDescriptorSignifTtest (dico_ttest, pvalue=0.10)
            list_descriptor_ttest_signif = list_result_ttest_signif.keys () # because confidence interval is calculate
        print list_descriptor_ttest_signif
    
    
    if ACP : 
        path_result = pathDirectory.result (dataset_name + "/" + pocket_type + "/ACP")
        analysis.specificACP("global", dictionary_descriptor, path_result + "des_global", mainACP = "Descriptor_global")
#         analysis.specificACP (l_consensus, dictionary_descriptor, path_result + "consensus_PPE", mainACP = "concensusPPE")
#        if 'list_descriptor_ttest_signif' in locals () :
#            analysis.specificACP(list_descriptor_ttest_signif, dictionary_descriptor, path_result + "signifttest", mainACP = "T-test_signif")
#            
#        analysis.specificACP("volume", dictionary_descriptor, path_result + "des_volume", mainACP = "Descriptor_volume")
#        analysis.specificACP("atomic", dictionary_descriptor, path_result + "des_atomic", mainACP = "Descriptor_atomic")
#         analysis.specificACP("fpocket", dictionary_descriptor, path_result + "des_fpocket", mainACP = "Descriptor_Fpocket")  
#        analysis.specificACP(["RADIUS_HULL", "DIAMETER_HULL", "SURFACE_HULL", "VOLUME_HULL", "SMALLEST_SIZE", "INERTIA_3", "INERTIA_1", "FACE", "HEIGHT_CYLINDER", "PCI", "PSI", "RADIUS_CYLINDER", "%_ATOM_CONVEXE", "CONVEX-SHAPE_COEFFICIENT", "INERTIA_2", "c_residues", "c_atom", "Real_volume"], dictionary_descriptor, path_result + "des_geo", mainACP = "Descriptor_Fpocket") 
#         analysis.specificACP(["C_residues", "C_atom"], dictionary_descriptor, path_result + "des_count", mainACP = "Descriptor_count") 

        analysis.specificACP(["VOLUME_HULL", "SMALLEST_SIZE", "RADIUS_HULL", "DIAMETER_HULL" ,"SURFACE_HULL", "RADIUS_CYLINDER" , "C_ATOM", "C_RESIDUES", 
                              "PSI", "INERTIA_2", "INERTIA_3", "INERTIA_1", "CONVEX.SHAPE_COEFFICIENT", "PCI", "FACE", "X._ATOM_CONVEXE"],
                             dictionary_descriptor, path_result + "des_vol_form", mainACP = "Descriptor_vol_form")
         
        analysis.specificACP(["hydrophobic_kyte", "p_hydrophobic_residues" ,"p_hydrophobic_atom", "p_hyd_atom","hydrophobicity_pocket_pocket", "p_aromatic_residues", 
                              "p_Car_atom", "p_polar_residues" ,"p_aliphatic_residues","p_Nlys_atom", "p_Ntrp_atom", "p_S_atom", "p_Otyr_atom", "p_Ooh_atom",
                               "p_O_atom", "p_N_atom", "p_ND1_atom", "p_NE2_atom", "polarity_pocket_pocket", "p_charged_residues", "p_positive_residues", "p_negative_residues",
                               "p_Ocoo_atom", "p_Cgln_atom",  "p_Ccoo_atom", "p_Carg_atom", "charge",  "p_pro_residues", "p_tiny_residues",  "p_main_chain_atom", 
                               "p_side_chain_atom", "P_C_atom", "p_nitrogen_atom", "p_sulfur_atom", "p_oxygen_atom", "p_carbone_atom"], dictionary_descriptor, path_result + "des_physico", mainACP = "Descriptor_des_physico")

#         if pocket_type == "Fpocket" : 
#             analysis.specificACP (list_without_ligand, dictionary_descriptor,  path_result +"without_ligand", "descriptor_selected") 
#         else : 
#             analysis.specificACP (list_with_ligand,dictionary_descriptor, path_result +"with_ligand", "descriptor_selected") 
     
     
     
        
    if LDAGlobal :
        # path directory
        path_begin = pathDirectory.result (dataset_name + "/" + pocket_type + "/LDA")
        # global data for Rscript
        path_global_descriptor = writeFiles.globalDescriptors(dictionary_descriptor, path_begin + "global.data")
        
        if selection_variable == 1 : 
            path_dir_parameter = pathDirectory.generatePath(path_begin + "selectedDesc_" + str (nb_desc_model) + "/")
            list_path_files = writeFiles.specificDescriptorbyData(dictionary_descriptor, "global", path_dir_parameter + "global_desc")
            list_descriptor_selected = parameters.retrieveDescriptorForLDAModel (dictionary_descriptor, list_path_files[0], list_path_files[1], path_dir_parameter,  criteria_train = criteria_train, criteria_loo = criteria_loo, criteria_CV = criteria_CV, criteria_test = criteria_test, nb_desc_model = nb_desc_model)   
        
#         analysis.LDAGlobal (dictionary_descriptor, "global", pathDirectory.generatePath(path_begin + "DescGlobal/"), "des_global", "Descriptor_global", test_train_data = 1, name_dataset = dataset_name, path_file_global_descriptor = path_global_descriptor)
#        analysis.LDAGlobal (dictionary_descriptor, "radi", pathDirectory.generatePath(path_begin + "DescRadi/"), "des_radi", "Descriptor_radi",test_train_data = 1, name_dataset = dataset_name, path_file_global_descriptor = path_global_descriptor)
#       # descriptor selected
#         if "list_descriptor_selected" in locals () :
#             analysis.LDAGlobal (dictionary_descriptor, list_descriptor_selected, pathDirectory.generatePath(path_begin + "AutoSelected/"), "autoselected", "autoselected", path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#        
#        # Descriptors imposed
        analysis.LDAGlobal (dictionary_descriptor, ["p_pro_residues", "X._ATOM_CONVEXE", "VOLUME_HULL", "hydrophobic_kyte", "polarity_pocket_pocket", "p_aromatic_residues", "p_Car_atom" ], pathDirectory.generatePath(path_begin + "Course/"), "Course", "Course",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)

        
#         if pocket_type == "Fpocket" : 
#             analysis.LDAGlobal (dictionary_descriptor, l_M1_test, pathDirectory.generatePath(path_begin + "M1/"), "M1", "M1",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M2_test, pathDirectory.generatePath(path_begin + "M2/"), "M2", "M2",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M3_test, pathDirectory.generatePath(path_begin + "M3/"), "M3", "M3",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M4_test, pathDirectory.generatePath(path_begin + "M4/"), "M4", "M4",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M5_test, pathDirectory.generatePath(path_begin + "M5/"), "M5", "M5",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M6_test, pathDirectory.generatePath(path_begin + "M6/"), "M6", "M6",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M7_test, pathDirectory.generatePath(path_begin + "M7/"), "M7", "M7",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M8_test, pathDirectory.generatePath(path_begin + "M8/"), "M8", "M8",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M9_test, pathDirectory.generatePath(path_begin + "M9/"), "M9", "M9",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M10_test, pathDirectory.generatePath(path_begin + "M10/"), "M10", "M10",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M11_test, pathDirectory.generatePath(path_begin + "M11/"), "M11", "M11",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M12_test, pathDirectory.generatePath(path_begin + "M12/"), "M12", "M12",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M13_test, pathDirectory.generatePath(path_begin + "M13/"), "M13", "M13",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M14_test, pathDirectory.generatePath(path_begin + "M14/"), "M14", "M14",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M15_test, pathDirectory.generatePath(path_begin + "M15/"), "M15", "M15",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M16_test, pathDirectory.generatePath(path_begin + "M16/"), "M16", "M16",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M17_test, pathDirectory.generatePath(path_begin + "M17/"), "M17", "M17",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M18_test, pathDirectory.generatePath(path_begin + "M18/"), "M18", "M18",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M19_test, pathDirectory.generatePath(path_begin + "M19/"), "M19", "M19",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M20_test, pathDirectory.generatePath(path_begin + "M20/"), "M20", "M20",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M21_test, pathDirectory.generatePath(path_begin + "M21/"), "M21", "M21",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)



# 
# 
# #            
# #             
#         elif pocket_type == "proximity" : 
#             analysis.LDAGlobal (dictionary_descriptor, l_M1_prox, pathDirectory.generatePath(path_begin + "M1/"), "M1", "M1",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M2_prox, pathDirectory.generatePath(path_begin + "M2/"), "M2", "M2",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M3_prox, pathDirectory.generatePath(path_begin + "M3/"), "M3", "M3",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M4_prox, pathDirectory.generatePath(path_begin + "M4/"), "M4", "M4",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M5_prox, pathDirectory.generatePath(path_begin + "M5/"), "M5", "M5",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M6_prox, pathDirectory.generatePath(path_begin + "M6/"), "M6", "M6",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M7_prox, pathDirectory.generatePath(path_begin + "M7/"), "M7", "M7",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M8_prox, pathDirectory.generatePath(path_begin + "M8/"), "M8", "M8",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M10_prox, pathDirectory.generatePath(path_begin + "M10/"), "M10", "M10",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#             analysis.LDAGlobal (dictionary_descriptor, l_M11_prox, pathDirectory.generatePath(path_begin + "M11/"), "M11", "M11",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
       
#         analysis.LDAGlobal (dictionary_descriptor, ["hydrophobic_kyte","p_Ooh_atom","p_main_chain_atom"], pathDirectory.generatePath(path_begin + "best2/"), "without_ligand", "without_ligand",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#         analysis.LDAGlobal (dictionary_descriptor, ["hydrophobic_kyte", "p_Carg_atom", "p_aromatic_residues"], pathDirectory.generatePath(path_begin + "best3/"), "without_ligand", "without_ligand",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)

       
        
        # test possibility with less descriptors
#        analysis.LDAGlobal (dictionary_descriptor, ["hydrophobic_kyte"], pathDirectory.generatePath(path_begin + "hydroOnly/"), "hydroOnly", "hydroOnly",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#         analysis.LDAGlobal (dictionary_descriptor, ["hydrophobic_kyte", "p_aromatic_residues"], pathDirectory.generatePath(path_begin + "hydroAro/"), "hydroAro", "hydroAro",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#         analysis.LDAGlobal (dictionary_descriptor, ["hydrophobic_kyte", "RADIUS_CYLINDER"], pathDirectory.generatePath(path_begin + "hydroRadius/"), "hydroRadius", "hydroRadius",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#         analysis.LDAGlobal (dictionary_descriptor, ["hydrophobic_kyte", "PCI", "RADIUS_CYLINDER"], pathDirectory.generatePath(path_begin + "hydroRadiusPCI/"), "hydroRadiusPCI", "hydroRadiusPCI",  path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)


        
    if CART : 
        path_begin = dataset_name + "/" + pocket_type + "/CART/"
        analysis.cart(dictionary_descriptor, "global", pathDirectory.result (path_begin + "des_global") + "des_global")
        
#      
        if "list_descriptor_ttest_signif" in locals () :
            analysis.cart (dictionary_descriptor, list_descriptor_ttest_signif, pathDirectory.result (path_begin + "des_Ttest") + "des_Ttest")
            
        if pocket_type == "Fpocket" : 
            analysis.cart (dictionary_descriptor, list_without_ligand, pathDirectory.result (path_begin + "des_AutoSelect") +"des_select_desc") 
        else : 
            analysis.cart(dictionary_descriptor, list_with_ligand,  pathDirectory.result (path_begin + "des_AutoSelect") +"des_select_desc") 
            
    if radomForest : 
        
        path_begin = dataset_name + "/" + pocket_type + "/RANDOMFOREST/"
        analysis.radomForest(dictionary_descriptor, "global", pathDirectory.result (path_begin + "des_global") + "des_global")
        
    if regLog : 
        
        path_begin = dataset_name + "/" + pocket_type + "/RegLog/"
        analysis.glm(dictionary_descriptor, "global", pathDirectory.result (path_begin + "des_global") + "des_global")
        analysis.glm(dictionary_descriptor, ["p_pro_residues", "X._ATOM_CONVEXE", "VOLUME_HULL", "hydrophobic_kyte", "polarity_pocket_pocket", "p_aromatic_residues", "p_Car_atom" ], pathDirectory.result (path_begin + "course") + "course")
        
    
    if correlation : 
        path_result = pathDirectory.result (dataset_name + "/" + pocket_type + "/CorDescriptor")
        analysis.correlationDescriptor (dictionary_descriptor, "global", path_result + "global_desc")
        
        # radi
        analysis.correlationDescriptor (dictionary_descriptor, "radi", path_result + "radi_desc")
        
        # aromatic
        analysis.correlationDescriptor (dictionary_descriptor, l_arom, path_result + "arom_desc")

        if "list_descriptor_ttest_signif" in locals () :
            analysis.correlationDescriptor (dictionary_descriptor, list_descriptor_ttest_signif,  path_result +"des_signiftest")
#      
        if pocket_type == "Fpocket" : 
            analysis.correlationDescriptor (dictionary_descriptor, list_without_ligand,  path_result +"des_select_desc") 
        else : 
            analysis.correlationDescriptor (dictionary_descriptor, list_with_ligand,  path_result +"des_select_desc") 
    
    if histogram : 
        path_begin = pathDirectory.result (dataset_name + "/" + pocket_type + "/Distribution")
        
        path_global_descriptor = writeFiles.globalDescriptors(dictionary_descriptor, path_begin + "global.data")
        
        runOtherProg.histogram (path_global_descriptor, path_begin, "drugg") 
        runOtherProg.MeansSDMaxMin (path_begin + "global.data",path_begin, pocket_type)
        #try :
        #    list_path_files = writeFiles.specificDescriptorbyData(dictionary_descriptor, "global", path_begin + "global_desc")
        
        #    runOtherProg.histogram (list_path_files[0], path_begin, "drugg")
        #    runOtherProg.histogram (list_path_files[1], path_begin, "drugg")
        #except:
        #    pass
     
    if SVM : 
        # path directory
        path_begin = pathDirectory.result (dataset_name + "/" + pocket_type + "/SVM")
        # global data for Rscript
        path_global_descriptor = writeFiles.globalDescriptors(dictionary_descriptor, path_begin + "global.data")
        
        
        if selection_variable == 1 :
            path_dir_parameter = pathDirectory.generatePath(path_begin + "selectedDesc/")
#             print path_dir_parameter
#            
            list_path_files = writeFiles.specificDescriptorbyData(dictionary_descriptor, "global", path_dir_parameter + "global_desc")
            p_file_every_models = parameters.retrieveDescriptorForSVMModel (dictionary_descriptor, path_dir_parameter, nb_descriptor = 4, debug = 0)
            list_descriptor_selected = parameters.selectBestModelAmongBestModel (dictionary_descriptor, p_file_every_models, 0.65, type_learning="SVM")
#        
        analysis.SVM (dictionary_descriptor, "global", pathDirectory.generatePath(path_begin + "global/"), "global", path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)

        
#       # descriptor selected
        if "list_descriptor_selected" in locals () :
            analysis.SVM (dictionary_descriptor, list_descriptor_selected, pathDirectory.generatePath(path_begin + "AutoSelected/"), "autoselected", path_file_global_descriptor = path_global_descriptor, name_dataset = dataset_name)
#        
      
      

def analysisPocketEstimation (dictionnary_dataset, name_dataset, retrieve_type_pocket) :
    """
    Global analysis of pocket estimation (numbers pockets, ....)
    args: -> dictionary data set
          -> name data set to generate directory
    return: NONE, files results in directory results 
    
    """
    # number of pocket by PDB
    analysis.dataSetNumberPockets(dictionnary_dataset.keys (), name_dataset, retrieve_type_pocket)
    # distance between pockets
    checkPocket.distancePocket(dictionnary_dataset.keys(), pathDirectory.dataSet(name_dataset) ,pathDirectory.result(name_dataset + "/dataset/" + retrieve_type_pocket) + "distance_pocket")
    tool.generateFilePDBWithChain (dictionnary_dataset, pathDirectory.result(name_dataset + "/dataset/" + retrieve_type_pocket) + "list_PDB_chain", retrieve_type_pocket, name_dataset)
    tool.concatenePocket (dictionnary_dataset, pathDirectory.result(name_dataset + "/dataset/"+ retrieve_type_pocket) + "pocketsAll",name_dataset, retrieve_type_pocket, option_only_on_pocket = 1)
    


        
def analysisDataset (dictionnary_dataset, name_dataset):
    """
    Analyse dataset DD between drug score and the confidence
    args: -> dictionnary dataset
          -> name dataset
    return: NULL draw histogram with drug score and confidence
    """
    analysis.resolution (dictionnary_dataset.keys(), name_dataset )
    if name_dataset == "DD" : 
        analysis.histCarac(dictionnary_dataset, name_dataset, "Drug Score")
        analysis.histCarac(dictionnary_dataset, name_dataset, "Confidence")
    
    
    
def calculDatasetDictionary (name_dataset, genrate_files = 0, pocket_remove = 0, debug = 0):
    
    path_file_krasowski = pathDirectory.dataSet() + "krasowski_2011_dataset.txt"
    path_file_Schmidtke = pathDirectory.dataSet() + "DD_Schmitke.csv"
    path_file_apo_Schmitke_clean = pathDirectory.dataSet() + "apoClean.txt"
    path_file_Perola_phase3 = pathDirectory.dataSet() + "perola_phase3.txt"
    path_file_Perola = pathDirectory.dataSet() + "perola_drugg.txt"
    path_file_huang = pathDirectory.dataSet() + "apoCleanHuang.csv"
    dir_dataset = pathDirectory.dataSet ( name_dataset )
    
    if re.search ("krasowski", name_dataset) :
        dir_dataset = pathDirectory.dataSet ("krasowski")
        dico_dataset = dataSet.formatFileDataSetKrasowki2011(path_file_krasowski)
        if debug : print dico_dataset 
        if genrate_files : 
            for PDB_ID in dico_dataset.keys() :
                generateDataset (PDB_ID, dir_dataset)
            #identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset) # ancien protocole avec prog de leslie, maintenant generation direct -> water
            
        dataSet.compareDatasetKrasowskiSchmitke(dico_dataset, path_file_Schmidtke,dir_dataset, debug = 0)
        #if debug : print dico_dataset 
        dataSet.analysisLigandDataSet(dico_dataset, dir_dataset) # lipinski rule selected
        #if debug : print dico_dataset 
        dataSet.manualAnalysisForKrasowskiDataSet(dico_dataset)
        if debug : print len(dico_dataset.keys ()), ">TEST<"
        
        if name_dataset != "krasowski" :  # case retrieve only pocket train or test
            dataSet.diviseTrainTest (dico_dataset,name_dataset.split ("_")[-1] )
        
        
    elif name_dataset == "DD" : 
        dir_dataset = pathDirectory.dataSet ("DD")
        dico_dataset = dataSet.formatFileDataSchmitkeDD(path_file_Schmidtke)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
            identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset)
    
    elif name_dataset == "Perola" : 
        dir_dataset = pathDirectory.dataSet ("Perola")
        dico_dataset = dataSet.formatFileDataPerola(path_file_Perola)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
                
    elif name_dataset == "PerolaPhase3" : 
        dir_dataset = pathDirectory.dataSet ("PerolaPhase3")
        dico_dataset = dataSet.formatFileDataPerolaPhase3(path_file_Perola_phase3)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
    
    elif name_dataset == "ApoForm" : 
        dir_dataset = pathDirectory.dataSet ("ApoForm")
        dico_dataset = dataSet.formatFileDataSchmitkeDD(path_file_Schmidtke)
        dico_dataset = dataSet.retrieveApoHoloFormDD (dico_dataset)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
            identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset)    
    
    elif name_dataset == "ApoForm128" : 
        dir_dataset = pathDirectory.dataSet ("ApoForm128")
        dico_dataset = dataSet.formatFileDataSchmitkeDD(path_file_Schmidtke)
        dico_dataset = dataSet.retrieveApoHoloFormDD (dico_dataset, l_PDB = dataSet.l_apo128)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
            identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset)  
    
    elif name_dataset == "ApoForm138" : 
        dir_dataset = pathDirectory.dataSet ("ApoForm138")
        dico_dataset = dataSet.formatFileDataSchmitkeDD(path_file_Schmidtke)
        dico_dataset = dataSet.retrieveApoHoloFormDD (dico_dataset, l_PDB = dataSet.l_apo138)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
            identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset)  
    
    
    elif name_dataset == "ApoFormClean" : 
        dir_dataset = pathDirectory.dataSet ("ApoFormClean")
        dico_dataset = dataSet.formatFileData(path_file_apo_Schmitke_clean)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
            identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset)       
    
    elif name_dataset == "ApoHuang" : 
        dir_dataset = pathDirectory.dataSet ("ApoHuang")
        dico_dataset = dataSet.formatFileDataSchmitkeDD(path_file_huang)
        dico_dataset = dataSet.retrieveApoHoloFormDD (dico_dataset)
        if genrate_files :
            for PDB_ID in dico_dataset.keys () : 
                generateDataset (PDB_ID, dir_dataset)
            identityCalcul.pasteFastaFileGlobal(dico_dataset.keys (), dir_dataset)     
          
    # Write dataset file (PDB with ligand pocket)       
    if debug : print dico_dataset 
#    writeFiles.datasetWithLigand(dico_dataset, dir_dataset + "dataSet.txt")

    # remove pocket not predicted by Fpocket
    if pocket_remove == 1 : 
        l_PDB_remove = ["3CAJ", "1BWN", "1AI2", "1QS4", "1RNT", "1OQ5"]
        l_pdb = dico_dataset.keys ()
        nb_dico = len (l_pdb)
        
        i = 0
        while i < nb_dico :
            if l_pdb[i] in l_PDB_remove : 
                del dico_dataset[l_pdb[i]]
                del l_pdb[i]
                nb_dico = nb_dico -1
            else : 
                i = i + 1
            

    return dico_dataset


def generateDataset (PDB_ID, dir_dataset):
    
    if downloadFile.importPDB( PDB_ID, dir_dataset) == 0 : 
        print "ERROR retrieve PDB files"
        return
    preparePDB.AllPDBSepareChain(PDB_ID, dir_dataset)
    #preparePDB.AllPDBProtonation(PDB_ID, dir_dataset) # -> protonation only protein.pdb not use now
    if downloadFile.importFasta(PDB_ID, dir_dataset) == 0 :
        #again
        downloadFile.importFasta(PDB_ID, dir_dataset)
        
    preparePDB.separeChainFasta (PDB_ID, dir_dataset)
    
    
def pocketEstimation (name_dataset, dictionary_dataset, pocket_type_retrieve, runFpocket = 1, file_dir_name = 1):
    """
    Estimation pocket
    args: -> name dataset
          -> dictionary dataset
          -> run Fpocket (run estimation)
          -> run Surflex (generate protomol)
          -> run NACCESS (generate protomol)
    return: NONE write files
    """
    
    dir_dataset = pathDirectory.dataSet(name_dataset)
    if re.search("prox",pocket_type_retrieve) :
        thresold =  pocket_type_retrieve.replace ("prox", "")
        checkPocket.selectPocketProximity(dictionary_dataset, dir_dataset, name_dataset, pocket_type_retrieve, file_dir_name, thresold_estimation = float(thresold)) 
    elif pocket_type_retrieve == "cavitator" : 
        runOtherProg.globalCavitator(dir_dataset, dictionary_dataset.keys(), debug = 1)
    elif pocket_type_retrieve == "DogSite" : 
        checkPocket.retrievePocketDogSite (dir_dataset, dictionary_dataset.keys(), debug = 1)
        checkPocket.selectPocketDogSite(dictionary_dataset, dir_dataset,  name_dataset, pocket_type_retrieve, file_dir_name = file_dir_name) 
    else : 
        if runFpocket == 1 : 
            runOtherProg.globalFpocket(dir_dataset, dictionary_dataset.keys ())
        checkPocket.selectPocketFpocket(dictionary_dataset, dir_dataset,  name_dataset, pocket_type_retrieve, option_aggregation_pocket = 0, file_dir_name = file_dir_name)


def pocketEstimationApoForm (name_dataset, dictionary_dataset_apo, dictionary_dataset_holo, pocket_retrieve_type = "Fpocket", runFpocket = 1):
    
    # run Fpocket
    dir_dataset = pathDirectory.dataSet(name_dataset)
    if runFpocket == 1 : 
        runOtherProg.globalFpocket(dir_dataset, dictionary_dataset_apo.keys ())
    
    if pocket_retrieve_type == "DogSite" : 
        checkPocket.retrievePocketDogSite (dir_dataset, dictionary_dataset_apo.keys(), debug = 1)
    
    checkPocket.selectPocketApo(dictionary_dataset_apo, dictionary_dataset_holo, dir_dataset, name_dataset, pocket_retrieve_type)



def generationProtomol (dictionary_dataset, pocket_type_retrieve, name_dataset):
    """
    Generate protomol with Surflex
    args: -> name data setname_dataset, pocket_type_retrieve
          -> dictionary data set
    return: -> generate Surflex protomol
    """
    dir_dataset = pathDirectory.dataSet(name_dataset)
    runOtherProg.protomolGenerationSurflexe (dictionary_dataset,  dir_dataset, pocket_type_retrieve, name_dataset )



def retrieveGlobalDescriptors (pocket_retrieve_type, protomol_type, dico_dataset, name_dataset, write_file = 1, calcul_descriptor = 0, option_separate = 1, compo_aa = 1):
    """
    Retrieve for every PDB in dico dataset values of descriptors
    args: -> type pocket retrieve
          -> protomol type
          -> dictionary dataset
          -> directory data set
    return: -> dictionary with descriptor
    """
    
    try : name_dataset = name_dataset.split ("_")[0]
    except  : pass
    
    
    
    dir_dataset = pathDirectory.dataSet(name_dataset)
    
    if calcul_descriptor == 1 : 
        getResidues.globalGetCompositionPocket (dico_dataset, dir_dataset, pocket_retrieve_type, name_dataset, debug = 1)
        descriptor.runDescriptorGlobal(dico_dataset, dir_dataset, protomol_type, pocket_retrieve_type, name_dataset, compo_aa = compo_aa)
    return loadDescriptors.generalLoadDescriptor(dico_dataset, protomol_type, pocket_retrieve_type, name_dataset, write_file=write_file, option_separate=option_separate)



def ACPDataset1BetwwenDataset2 (name_dataset1, name_dataset2, pocket_retrive_type1, protomol_type1, pocket_retrieve_type2, protomol_type2,  type_pocket = "all", list_descriptor = "global"):
    
    
    
    path_result = pathDirectory.result(name_dataset1 + "_" + name_dataset2 + "-" + pocket_retrive_type1 + "_" + pocket_retrieve_type2)
    dataset1 = calculDatasetDictionary(name_dataset1, 0)
    dataset2 = calculDatasetDictionary(name_dataset2, 0)
    
    dico_data1= retrieveGlobalDescriptors (pocket_retrive_type1, protomol_type1, dataset1, name_dataset1, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data2= retrieveGlobalDescriptors (pocket_retrieve_type2, protomol_type2, dataset2, name_dataset2, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
#    print dico_data1["No-Druggable"]["PDB"]
#    print dico_data2["No-Druggable"]["PDB"]
#    list_union = list(set(dico_data2["No-Druggable"]["PDB"])& set( dico_data1["No-Druggable"]["PDB"]))
#    print list_union
    
    path_file_data1 = path_result + name_dataset1 + "1"
    path_file_data2 = path_result + name_dataset2 + "2"
    path_file_result = path_result + name_dataset1 + "_" + name_dataset2
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
    if list_descriptor == "global" : 
        writeFiles.globalDescriptors(dico_data1, path_file_data1, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data2, path_file_data2, type_pocket = type_pocket)
    else : 
        writeFiles.specificDescriptor(dico_data1, list_descriptor, path_file_data1, debug = 1 )
        writeFiles.specificDescriptor(dico_data2, list_descriptor, path_file_data2, debug = 1 )
    
    runOtherProg.ACPDataset (path_file_data1, path_file_data2, path_file_result)



def ACPDatasetBetwwen3Dataset (name_dataset1, name_dataset2, name_dataset3, pocket_retrive_type1, protomol_type1, pocket_retrieve_type2, protomol_type2, pocket_retrieve_type3, protomol_type3, type_pocket = "all", list_descriptor = "global"):

    
    
    path_result = pathDirectory.result(name_dataset1 + "_" + name_dataset2  + "_" + name_dataset3 + "-" + pocket_retrive_type1 + "_" + pocket_retrieve_type2 + "_" + pocket_retrieve_type3)
    dataset1 = calculDatasetDictionary(name_dataset1, 0)
    dataset2 = calculDatasetDictionary(name_dataset2, 0)
    dataset3 = calculDatasetDictionary(name_dataset3, 0)
    
    dico_data1= retrieveGlobalDescriptors (pocket_retrive_type1, protomol_type1, dataset1, name_dataset1, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data2= retrieveGlobalDescriptors (pocket_retrieve_type2, protomol_type2, dataset2, name_dataset2, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data3= retrieveGlobalDescriptors (pocket_retrieve_type3, protomol_type3, dataset3, name_dataset3, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
#    print dico_data1["No-Druggable"]["PDB"]
#    print dico_data2["No-Druggable"]["PDB"]
#    list_union = list(set(dico_data2["No-Druggable"]["PDB"])& set( dico_data1["No-Druggable"]["PDB"]))
#    print list_union
    
    path_file_data1 = path_result + name_dataset1 + "1"
    path_file_data2 = path_result + name_dataset2 + "2"
    path_file_data3 = path_result + name_dataset2 + "3"
    
    path_file_result = path_result + name_dataset1 + "_" + name_dataset2 + "_" +name_dataset3
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
    if list_descriptor == "global" : 
        writeFiles.globalDescriptors(dico_data1, path_file_data1, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data2, path_file_data2, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data3, path_file_data3, type_pocket = type_pocket)
    else : 
        writeFiles.specificDescriptor(dico_data1, list_descriptor, path_file_data1, debug = 1 )
        writeFiles.specificDescriptor(dico_data2, list_descriptor, path_file_data2, debug = 1 )
        writeFiles.specificDescriptor(dico_data3, list_descriptor, path_file_data3, debug = 1 )
    
    runOtherProg.ACP3Dataset (path_file_data1, path_file_data2, path_file_data3, path_file_result)



def ACPDatasetBetween4Dataset (name_dataset1, name_dataset2, name_dataset3, name_dataset4, pocket_retrive_type1, protomol_type1, pocket_retrieve_type2, protomol_type2, pocket_retrieve_type3, protomol_type3, pocket_retrieve_type4, protomol_type4, type_pocket = "all", list_descriptor = "global"):


    path_result = pathDirectory.result(name_dataset1 + "_" + name_dataset2  + "_" + name_dataset3 + "_" + name_dataset4 + "-" + pocket_retrive_type1 + "_" + pocket_retrieve_type2 + "_" + pocket_retrieve_type3 + "_" + pocket_retrieve_type4)
    dataset1 = calculDatasetDictionary(name_dataset1, 0)
    dataset2 = calculDatasetDictionary(name_dataset2, 0)
    dataset3 = calculDatasetDictionary(name_dataset3, 0)
    dataset4 = calculDatasetDictionary(name_dataset4, 0)
    
    dico_data1= retrieveGlobalDescriptors (pocket_retrive_type1, protomol_type1, dataset1, name_dataset1, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data2= retrieveGlobalDescriptors (pocket_retrieve_type2, protomol_type2, dataset2, name_dataset2, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data3= retrieveGlobalDescriptors (pocket_retrieve_type3, protomol_type3, dataset3, name_dataset3, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data4= retrieveGlobalDescriptors (pocket_retrieve_type4, protomol_type4, dataset4, name_dataset4, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
#    print dico_data1["No-Druggable"]["PDB"]
#    print dico_data2["No-Druggable"]["PDB"]
#    list_union = list(set(dico_data2["No-Druggable"]["PDB"])& set( dico_data1["No-Druggable"]["PDB"]))
#    print list_union
    
    path_file_data1 = path_result + name_dataset1 + "1"
    path_file_data2 = path_result + name_dataset2 + "2"
    path_file_data3 = path_result + name_dataset3 + "3"
    path_file_data4 = path_result + name_dataset4 + "4"
    
    path_file_result = path_result + name_dataset1 + "_" + name_dataset2 + "_" +name_dataset3 + "_" + name_dataset4
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
    if list_descriptor == "global" : 
        writeFiles.globalDescriptors(dico_data1, path_file_data1, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data2, path_file_data2, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data3, path_file_data3, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data4, path_file_data4, type_pocket = type_pocket)
    else : 
        writeFiles.specificDescriptor(dico_data1, list_descriptor, path_file_data1, debug = 1 )
        writeFiles.specificDescriptor(dico_data2, list_descriptor, path_file_data2, debug = 1 )
        writeFiles.specificDescriptor(dico_data3, list_descriptor, path_file_data3, debug = 1 )
        writeFiles.specificDescriptor(dico_data4, list_descriptor, path_file_data4, debug = 1 )
    
    runOtherProg.PCA4Dataset (path_file_data1, path_file_data2, path_file_data3, path_file_data4, path_file_result)





def correlationDataset1BetwwenDataset2 (name_dataset1, name_dataset2, pocket_retrive_type1, protomol_type1, pocket_retrieve_type2, protomol_type2,  type_pocket = "all", list_descriptor = "global") : 
    
    
    path_result = pathDirectory.result(name_dataset1 + "_" + name_dataset2 + "-" + pocket_retrive_type1 + "_" + pocket_retrieve_type2)
    dataset1 = calculDatasetDictionary(name_dataset1, 0)
    dataset2 = calculDatasetDictionary(name_dataset2, 0)
    
    dico_data1= retrieveGlobalDescriptors (pocket_retrive_type1, protomol_type1, dataset1, name_dataset1, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data2= retrieveGlobalDescriptors (pocket_retrieve_type2, protomol_type2, dataset2, name_dataset2, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
#    print dico_data1["No-Druggable"]["PDB"]
#    print dico_data2["No-Druggable"]["PDB"]
#    list_union = list(set(dico_data2["No-Druggable"]["PDB"])& set( dico_data1["No-Druggable"]["PDB"]))
#    print list_union
    
    path_file_data1 = path_result + name_dataset1 + "1"
    path_file_data2 = path_result + name_dataset2 + "2"
    path_file_result = path_result + name_dataset1 + "_" + name_dataset2
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
    if list_descriptor == "global" : 
        writeFiles.globalDescriptors(dico_data1, path_file_data1, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data2, path_file_data2, type_pocket = type_pocket)
    else : 
        writeFiles.specificDescriptor(dico_data1, list_descriptor, path_file_data1, debug = 1 )
        writeFiles.specificDescriptor(dico_data2, list_descriptor, path_file_data2, debug = 1 )
    
    runOtherProg.cardDataset (path_file_data1, path_file_data2, path_file_result)





def MDSDataset1BetwwenDataset2 (name_dataset1, name_dataset2, pocket_retrive_type1, protomol_type1, pocket_retrieve_type2, protomol_type2,  type_pocket = "all", list_descriptor = "global"):
    
    
    
    path_result = pathDirectory.result(name_dataset1 + "_" + name_dataset2 + "-" + pocket_retrive_type1 + "_" + pocket_retrieve_type2)
    dataset1 = calculDatasetDictionary(name_dataset1, 0)
    dataset2 = calculDatasetDictionary(name_dataset2, 0)
    
    dico_data1= retrieveGlobalDescriptors (pocket_retrive_type1, protomol_type1, dataset1, name_dataset1, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_data2= retrieveGlobalDescriptors (pocket_retrieve_type2, protomol_type2, dataset2, name_dataset2, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
#    print dico_data1["No-Druggable"]["PDB"]
#    print dico_data2["No-Druggable"]["PDB"]
#    list_union = list(set(dico_data2["No-Druggable"]["PDB"])& set( dico_data1["No-Druggable"]["PDB"]))
#    print list_union
    
    path_file_data1 = path_result + name_dataset1 + "1"
    path_file_data2 = path_result + name_dataset2 + "2"
    path_file_result = path_result + name_dataset1 + "_" + name_dataset2
    path_file_color = writeFiles.colorACPFile (dico_data1)
    
    if list_descriptor == "global" : 
        writeFiles.globalDescriptors(dico_data1, path_file_data1, type_pocket = type_pocket)
        writeFiles.globalDescriptors(dico_data2, path_file_data2, type_pocket = type_pocket)
    else : 
        writeFiles.specificDescriptor(dico_data1, list_descriptor, path_file_data1, debug = 1 )
        writeFiles.specificDescriptor(dico_data2, list_descriptor, path_file_data2, debug = 1 )
    
    runOtherProg.MDSDataset (path_file_data1, path_file_data2, path_file_result)






def correlationDescriptorsByTypePocket (name_dataset, pocket_type1, protomol_type1, pocket_type2, protomol_type2) : 
    """Correlation between two data of descriptor"""
    
    dico_dataset = calculDatasetDictionary(name_dataset, 0)
    path_dir_result = pathDirectory.result(name_dataset + "/" + pocket_type1 + "VS" + pocket_type2)
    
    dico_descriptor1 = retrieveGlobalDescriptors (pocket_type1, protomol_type1, dico_dataset, name_dataset)
    dico_descriptor2 = retrieveGlobalDescriptors (pocket_type2, protomol_type2, dico_dataset, name_dataset)
    
    writeFiles.globalDescriptors(dico_descriptor1, path_dir_result + "desc1")
    writeFiles.globalDescriptors(dico_descriptor2, path_dir_result + "desc2")
    
    path_correlation = analysis.correlationDotChart (dico_descriptor1, dico_descriptor2, path_dir_result)
    runOtherProg.dotChart(path_correlation)
    runOtherProg.plotCorrelation (path_dir_result + "desc1", path_dir_result + "desc2", path_dir_result + "correlationbydescriptor")


def validation (name_dataset_train, name_dataset_test, pocket_retrieve_type, protomol_type, pvalue = 0.1, cor_value = 1):
    """
    A revoir car plus utilise, moyen de passer avec un save.image -----> a voir
    
    
    """
    # file color for ACP and path
    path_result = pathDirectory.result ("modelDruggability" + "/" + name_dataset_train + "_" + name_dataset_test)

    # load dataset
    dico_dataset_train = calculDatasetDictionary(name_dataset_train, 0)
    dico_dataset_test = calculDatasetDictionary(name_dataset_test, 0)
    
    # load descriptor
    dico_descriptors_train = retrieveGlobalDescriptors (pocket_retrieve_type, protomol_type, dico_dataset_train, name_dataset_train, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    dico_descriptors_test = retrieveGlobalDescriptors (pocket_retrieve_type, protomol_type, dico_dataset_test, name_dataset_test, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
    # color ACP
    writeFiles.colorACPFile (dico_descriptors_train) # color for ACP
    
    # global file for R script
    path_file_global_train = writeFiles.globalDescriptors(dico_descriptors_train, "global.data") # for Ttest and histogram, open in R script
    path_file_global_test = writeFiles.globalDescriptors(dico_descriptors_test, "global_test.data")
    
    # retrieve descriptor significant mean score
    if len (dico_descriptors_train["Druggable"].keys()) > 1 and len (dico_descriptors_train["No-Druggable"].keys()) > 1 :  
        dico_ttest = analysis.ttest(dico_descriptors_train["Druggable"], dico_descriptors_train["No-Druggable"], path_result + "ttest_" + name_dataset_train )
        # descriptor positive ttest between druggable and non-druggable
        dico_IC_signif = analysis.retrieveDescriptorSignifTtest (dico_ttest, pvalue = pvalue)

    list_descriptor_ttest_signif = dico_IC_signif.keys ()
       
    # write data with significatives descriptor
#    path_signif_descriptor_train = writeFiles.specificDescriptor(dico_descriptors_train, list_descriptor_ttest_signif, path_result + name_dataset_train+ "_train" )
#    path_signif_descriptor_test = writeFiles.specificDescriptor(dico_descriptors_test, list_descriptor_ttest_signif, path_result + name_dataset_test+ "_test" )
    
    # a voir 
    
    
    # parameter in LDA value correlation -> elimcor
    #runOtherProg.accTrainTest(path_signif_descriptor_train, path_signif_descriptor_test, 0.4,  0.02)
    #return
    list_descriptor_selected, IC_descriptor = analysis.LDAGlobal (dico_descriptors_train, list_descriptor_ttest_signif, path_result, "model", name_dataset = name_dataset_train)
    
    # descriptor signif betwenn good predicted and bad predicted
    path_descriptor = path_result + "descriptor_signif_good_bad_predicted"
    filout_descriptor =open (path_descriptor, "w")
    for descriptor in IC_descriptor.keys () : 
        if descriptor == "%_ATOM_CONVEXE" : 
            descriptor = "X._ATOM_CONVEXE"
        elif descriptor == "Mean_alpha-sphere_SA" : 
            descriptor = "Mean_alpha.sphere_SA"
        filout_descriptor.write (descriptor + "\n")
    filout_descriptor.close ()
    
    dico_test_corecting = tool.selectData (dico_descriptors_test, IC_descriptor)
    
    
    list_predicted = analysis.LDAmodel(dico_descriptors_train, dico_descriptors_test, dico_test_corecting, list_descriptor_selected, path_file_global_train, path_file_global_test, 1, path_result, path_descriptor)
    
#    os.system ("rm " + path_file_global_test)
#    os.system ("rm " + path_file_global_train)
    
#    analysis.scoreBadPredict (list_predicted, dico_dataset_train, dico_dataset_test)


def applyModel(path_file_model, path_file_descriptor,  path_file_result, family_barplot = 0, name_dataset = "") :
    """
    Apply model LDA one new data
    args: -> path file model
          -> path file descriptors (new data)
          -> path file result
    return: path file result
    """
    if os.path.exists(path_file_result) : 
        os.remove(path_file_result)
    
    path_proba, path_result = runOtherProg.predictLDA (path_file_model, path_file_descriptor, path_file_result)
    
    if family_barplot : 
        dico_data = calculDatasetDictionary(name_dataset,0)
        filin = open (path_proba,"r")
        l_PDBscore = filin.readlines()[1:]
        filin.close ()
        filout = open (path_proba, "w")
        for PDBscore in l_PDBscore : 
            PDB = PDBscore.split (" ")[0].replace("\"", "")
            filout.write(str(PDB) + "\t" + str(PDBscore.split (" ")[1]) + "\t" + str(dico_data[PDB]["Protein name"]) + "\t" + str(dico_data[PDB]["druggability"]) + "\n")
        filout.close()
        
        runOtherProg.boxplotFamily (path_proba)
    
    
    return path_file_result
    
    
    



def correlationDescriptor (name_dataset, list_desc, pocket_retrive_type, protomol_type):
    
    dico_dataset = calculDatasetDictionary(name_dataset, 0)
    dico_descriptors = retrieveGlobalDescriptors (pocket_retrive_type, protomol_type, dico_dataset, name_dataset, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
    
    analysis.correlationBetween2Descriptor(dico_descriptors, list_desc)
    
    

def overlapTwoPockets (name_dataset, pocket_retrieve_type_1, pocket_retrieve_type_2): 
    
    
    path_dir_result = pathDirectory.result("overlapPocket" + str (pocket_retrieve_type_1) + "_" + str (pocket_retrieve_type_2))
    path_filout = path_dir_result + "histogramScore_" + name_dataset
    filout = open(path_filout, "w")
    filout.write ("PDB\tPO\tMO\tRO\tSOr1\tSOr2\tSOacc\tcomon\tnb1\tnb2\n")
    dico_dataset = calculDatasetDictionary(name_dataset, 0)
    dico_value = {}
    
    
    for PDB_ID in dico_dataset.keys () : 
        dico_value[PDB_ID] = {}
        list_dir_pocket1 = pathDirectory.generateListDirPocket (PDB_ID, pocket_retrieve_type_1, name_dataset)
        list_dir_pocket2 = pathDirectory.generateListDirPocket (PDB_ID, pocket_retrieve_type_2, name_dataset)
        if not list_dir_pocket1  or not  list_dir_pocket2  : 
            continue
        
        
        for dir_pocket1 in list_dir_pocket1 : 
            for dir_pocket2 in list_dir_pocket2 : 
                path_file_pocket1 = pathDirectory.searchPocketAtomFpocket(dir_pocket1)
                path_file_pocket2 = pathDirectory.searchPocketAtomFpocket(dir_pocket2)
                
                p_file_pocket1_asa = pathDirectory.searchPocketACC (dir_pocket1)
                p_file_pocket2_asa = pathDirectory.searchPocketACC (dir_pocket2)
                
                score_SO = overlapPocket.scoreOverlap(path_file_pocket1, path_file_pocket2)
                score_SOr1 = overlapPocket.scoreOverlapRealtive(path_file_pocket1, path_file_pocket2)
                score_SOr2 = overlapPocket.scoreOverlapRealtive(path_file_pocket2, path_file_pocket1)
                nb_commun_atom, nb_atom1, nb_atom2 = overlapPocket.communAtom(path_file_pocket1, path_file_pocket2)
                score_MO = overlapPocket.MO(path_file_pocket1, path_file_pocket2, p_file_pocket1_asa, p_file_pocket2_asa)
                score_RO = overlapPocket.MO(path_file_pocket2, path_file_pocket1, p_file_pocket2_asa, p_file_pocket1_asa)
                score_SOacc = overlapPocket.scoreOverlapAcc(path_file_pocket1, path_file_pocket2, p_file_pocket1_asa, p_file_pocket2_asa)
                
#                 dico_value[PDB_ID]["SO"] = score_SO
#                 dico_value[PDB_ID]["MO"]= score_MO
#                 dico_value[PDB_ID]["RO"] = score_RO
#                 dico_value[PDB_ID]["SOr1"] = score_SOr1
#                 dico_value[PDB_ID]["SOr2"] = score_SOr2
#                 dico_value[PDB_ID]["SOacc"] = score_SOacc
#                 dico_value[PDB_ID]["nb commun"] = nb_commun_atom
#                 dico_value[PDB_ID]["nb1"] = nb_atom1
#                 dico_value[PDB_ID]["nb2"] = nb_atom2
                   
                filout.write (str(PDB_ID) + "_" + path_file_pocket1.split("/")[-2].split ("pocket")[-1]  + "-" + path_file_pocket2.split("/")[-2].split ("pocket")[-1] + "\t" + str (score_SO) + "\t" + str (score_MO)+ "\t" + str (score_RO) + "\t" + str (score_SOr1) + "\t" + str (score_SOr2) + "\t" + str (score_SOacc) + "\t" + str (nb_commun_atom) + "\t" + str (nb_atom1) + "\t" + str (nb_atom2) + "\n")
   
    filout.close () 
    runOtherProg.histScoreByPDB (path_filout)
    
    
        

def SODataset (name_dataset, pocket_retrieve_type_1, pocket_retrieve_type_2):

    l_out_SO = []

    dico_dataset = calculDatasetDictionary(name_dataset, 0)
    
    
    for PDB_ID in dico_dataset.keys () : 
        list_dir_pocket1 = pathDirectory.generateListDirPocket (PDB_ID, pocket_retrieve_type_1, name_dataset)
        list_dir_pocket2 = pathDirectory.generateListDirPocket (PDB_ID, pocket_retrieve_type_2, name_dataset)
        if not list_dir_pocket1  or not  list_dir_pocket2  : 
            continue
        
        for dir_pocket1 in list_dir_pocket1 : 
            for dir_pocket2 in list_dir_pocket2 : 
                path_file_pocket1 = pathDirectory.searchPocketAtomFpocket(dir_pocket1)
                path_file_pocket2 = pathDirectory.searchPocketAtomFpocket(dir_pocket2)
                
                score_SO = overlapPocket.scoreOverlap(path_file_pocket1, path_file_pocket2)
                l_out_SO.append (score_SO)
                
    
    return l_out_SO          
   
    
    
    
    


def retrieveAccessibilityPocket(name_dataset, pocket_type) :
    
    path_dir_result = pathDirectory.result("accesibility" + str (pocket_type))
    path_filout = path_dir_result + "accessibility_pocket.out"
    filout = open(path_filout, "w")
    dico_dataset = calculDatasetDictionary(name_dataset, 0)
    
    for PDB_ID in dico_dataset.keys () : 
        path_pocket_asa = pathDirectory.searchPocketAtomASA(PDB_ID, name_dataset, pocket_type)
        if path_pocket_asa == None : continue
        acc = descriptorEnergy.accessibilityPocketbyAtom(path_pocket_asa)
        filout.write (PDB_ID + "\t" + str (acc) + "\n")
    
    filout.close ()
        
        

def bestmultiModelApo (p_filin) :
    
    filin = open (p_filin, "r")
    filin_read = filin.read ()
    
    list_model = filin_read.split ("@@@@@@@@@@@@@\"\n")
    
    print len(list_model), "88888"
    p_filout_plot = p_filin + "_plot"
    filout_plot = open (p_filout_plot, "w")
    
    p_filout_resume = p_filin + "_resume"
    filout_resume = open (p_filout_resume, "w")
    

    d_temp = {} # order by model

    regex_int = re.compile('[0-9]+')
    for model in list_model[:-1] : 
        model = model.split ("\n") 
        
        n_model = int(regex_int.findall (model[0])[-1])
        d_temp[n_model] = parseOutLDA.retrieveQualityLDA(model)
    
    
    n = 0    
    flag = 0
    while flag !=  len(d_temp.keys()) :  
        if not n in d_temp.keys () : 
            n = n + 1
            continue # case of best model selected.
        filout_plot.write ("M" + str ( n + 1) + "\t" + str (d_temp[n]["acc"]) + "\t" + str (d_temp[n]["mcc"]) + "\n")
        n = n + 1
        flag = flag + 1
        
    filout_plot.close ()
        
    runOtherProg.plotScoreCriterion (p_filout_plot)
    
    # summary
    print d_temp, "********"
    filout_resume.write ("acc: ")
    M,SD = tool.meansLModel (d_temp.values(), "acc")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("se: ")
    M,SD = tool.meansLModel (d_temp.values(), "se")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("sp: ")
    M,SD = tool.meansLModel (d_temp.values(), "sp")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    filout_resume.write ("mcc: ")
    M,SD = tool.meansLModel (d_temp.values(), "mcc")
    filout_resume.write (str(M) + "+/-" + str(SD) + "\n")
    
    
    filout_resume.close ()
    

def averageDruggProba (l_p_proba, p_filout):
    
    d_out = {}
    for p_proba in l_p_proba : 
        filin = open (p_proba, "r")
        l_lines_proba = filin.readlines()
        filin.close ()
        
        for line_proba in l_lines_proba[1:] : 
            name_pocket = line_proba.split (" ")[0]
            p_drug = float(line_proba.split (" ")[1])
    
            if not name_pocket in d_out.keys () : 
                d_out[name_pocket] = []
            d_out[name_pocket].append (p_drug)
        
    
    filout = open (p_filout, "w")
    for k in d_out.keys () : 
        filout.write (str (k) + "\t" + str(numpy.mean(d_out[k])) + "\t" + str (numpy.std(d_out[k])) + "\n")
    filout.close ()


def averageScoreDruggProba (l_p_probafile, p_filout) :
    
    
    d_pdb = {}
    
    for  p_file in l_p_probafile : 
        filin = open (p_file, "r")
        l_lines = filin.readlines ()
        filin.close ()
        
        for l_pdb in l_lines[1:] :
            PDB = l_pdb.split (" ")[0].replace ("\"", "")
            proba = float (l_pdb.split (" ")[1])
            real = int(l_pdb.strip().split (" ")[-1])
            
            if not PDB in d_pdb.keys () : 
                d_pdb[PDB] = {}
                d_pdb[PDB]["proba"] = []
                d_pdb[PDB]["real"] = []
            
            d_pdb[PDB]["proba"].append (proba)
            d_pdb[PDB]["real"].append (real)
    
    l_pdb = d_pdb.keys ()
    d_pdb["TP"] = 0.0
    d_pdb["TN"] = 0.0
    d_pdb["FN"] = 0.0
    d_pdb["FP"] = 0.0
    
    
    
    for pdb in l_pdb: 
        # mean
        means_prob = numpy.mean(d_pdb[pdb]["proba"])
        sd_prob = numpy.std(d_pdb[pdb]["proba"])
        d_pdb[pdb]["proba"] = means_prob
        d_pdb[pdb]["sd"] = sd_prob
        mean_real = numpy.mean(d_pdb[pdb]["real"])
        d_pdb[pdb]["real"] = mean_real
        
        if d_pdb[pdb]["proba"] >= 0.5 and d_pdb[pdb]["real"] == 2.0 : 
            d_pdb["TP"] = d_pdb["TP"] + 1 
        elif d_pdb[pdb]["proba"] < 0.5 and d_pdb[pdb]["real"] == 1.0 : 
            d_pdb["TN"] = d_pdb["TN"] + 1
        elif d_pdb[pdb]["proba"] >= 0.5 and d_pdb[pdb]["real"] == 1.0 : 
            d_pdb["FP"] = d_pdb["FP"] + 1
        elif d_pdb[pdb]["proba"] < 0.5 and d_pdb[pdb]["real"] == 2.0 : 
            d_pdb["FN"] = d_pdb["FN"] + 1
            
        else : 
            print "---ERROR l 866 globalFunction.py---"
    
    
    rateQuality.quality(d_pdb)
    
    filout = open (p_filout, "w")
    filout.write ("TN: " + str(d_pdb["TN"]) + "\n")
    filout.write ("TP: " + str(d_pdb["TP"]) + "\n")
    filout.write ("FN: " + str(d_pdb["FN"]) + "\n")
    filout.write ("FP: " + str(d_pdb["FP"]) + "\n")
    filout.write ("************************\n")
    filout.write ("ACC: " + str(d_pdb["acc"]) + "\n")
    filout.write ("PR: " + str(d_pdb["pr"]) + "\n")
    filout.write ("SE: " + str(d_pdb["se"]) + "\n")
    filout.write ("SP: " + str(d_pdb["sp"]) + "\n")
    filout.write ("MCC: " + str(d_pdb["mcc"]) + "\n")
    
    filout.write ("************************\n\n")
    for pdb in l_pdb : 
        filout.write (pdb + "\t" + str(d_pdb[pdb]["proba"]) + "\t" + str(d_pdb[pdb]["sd"]) + "\t" + str(d_pdb[pdb]["real"])  + "\n")
    
    
    
    filout.close ()
    




