"""
BORREL Alexandre
8-10-12
main to run descriptors
"""
# personnal modules
import getResidues
import descriptor
import runOtherProg
import globalFonction

# global modules
from os import system, path, makedirs, listdir
from re import search
from numpy.ma.core import indices


def formatTableDesc (pr_pocket, p_filout) : 
    
    d_out = {}
    filout = open (p_filout, "w")
    l_file = listdir (pr_pocket)
    for f in l_file : 
        if search(".desc", f) :
            d_desc = {} 
            filin = open (pr_pocket + f, "r")
            l_lines = filin.readlines ()
            for l in l_lines : 
                print l
                d = l.split ("\t")[0].split ("pocket_")[-1]
                if not d in d_desc.keys () : 
                    d_desc[d] = l.strip().split ("\t")[-1]
            d_out[f.split ("/")[-1].split (".")[0]] = d_desc

    print d_out

    l_des = d_out[d_out.keys ()[0]].keys ()
    filout.write ("\t".join (l_des) + "\n")    
    
    for name_pocket in d_out.keys () : 
        filout.write (name_pocket)
        for des in l_des : 
            filout.write ("\t" + str(d_out[name_pocket][des]))
        filout.write ("\n")
    filout.close ()
    
    return p_filout






def main (pr_pocket, p_complexe = "none", path_file_protomol = "none", name_ligand = "none", pr_Renv = "none"):
    
    
    l_pocket_file = listdir(pr_pocket)
    
    for pocket_file in l_pocket_file : 
        
        if search(".pdb", pocket_file) : 
            p_filout = pr_pocket +  pocket_file [0:-4] + ".desc"
            p_file_pocket = pr_pocket + pocket_file
            
            # temporaly directory
            pr_temp =pr_pocket + "/temp/"
        
            print pr_temp
            try :makedirs( pr_temp, mode=0777 )
            except :pass
    
    
    
            # run NACCESS
            if p_complexe != "none" : 
                p_files_naccess = runOtherProg.runNACESS(p_complexe)
            else :
                p_files_naccess = runOtherProg.runNACESS(p_file_pocket)
            
            p_file_ACC  =  getResidues.getAccAtom(p_files_naccess[0], p_file_pocket)
            p_file_pocket_res = getResidues.getPocketResidues(p_file_pocket, p_complexe, pr_pocket, pocket_file[0:-4])
            print p_file_pocket_res
        #    
        #    
            descriptor.runDescriptor(p_file_pocket, p_file_pocket_res[0], p_file_ACC[1], p_filout, p_complexe)
        
            system ("mv " + p_file_ACC[0] + " " + pr_temp + path.basename(p_file_ACC[0]) ) 
            system ("mv " + p_file_ACC[1] + " " + pr_temp + path.basename(p_file_ACC[1]) )
            system ("mv " + p_files_naccess[0] + " " + pr_temp + path.basename(p_files_naccess[0]) ) 
            system ("mv " + p_files_naccess[1] + " " + pr_temp + path.basename(p_files_naccess[1]) )
            
            
    
    
    p_desc_pocket = formatTableDesc (pr_pocket, pr_pocket + "table_global.txt")
    
    # run druggability model
    l_proba = []
    if pr_Renv != "none" : 
        l_file_model = listdir(pr_Renv)
        
        for file_model in l_file_model : 
            if search(".Rdata", file_model) : 
                out = runOtherProg.predictLDA(pr_Renv + file_model, p_desc_pocket, pr_pocket + file_model.split ("/")[-1][0:-6])
                l_proba.append (out[0])

    globalFonction.averageDruggProba (l_proba, pr_pocket + "druggable_table.txt")


######################
######## RUN #########
######################


pr_pockets = "/home/borrel/test/pockets/"
p_complex =  "/home/borrel/test/1ICJ.pdb"
pr_model = "./Models/"


main (pr_pockets, p_complex,  pr_Renv = pr_model)




