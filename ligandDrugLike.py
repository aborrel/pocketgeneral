
import pathDirectory
import runOtherProg




def retrieveSmileDrugLike (dico_dataset, path_filout, debug = 1):
    
    path_smile_code = path_filout + ".smi"
    path_list_ligand = path_filout + ".list"
    filout_smile = open (path_smile_code, "w")
    filout_ligand = open (path_list_ligand, "w")
    
    
    for PDB_ID in dico_dataset.keys () : 
        print PDB_ID
        
        path_dir_pocket = pathDirectory.generateListDirPocket(PDB_ID, "proximity", "krasowski")
        print path_dir_pocket
        path_pdb_ligand = pathDirectory.searchLigandPDB(PDB_ID, path_dir_pocket[0])
        path_file_smi = runOtherProg.babelPDBtoSMI(path_pdb_ligand)
        code_smile = extractSMILECODE (path_file_smi)
        
        if debug : 
            print path_pdb_ligand
            print path_dir_pocket[0]
            print path_file_smi
        filout_ligand.write (PDB_ID + "\t" + dico_dataset[PDB_ID]["druggability"] + "\n")
        filout_smile.write (code_smile + "\n")
    
    filout_ligand.close ()
    filout_smile.close ()
    
    return [path_file_smi, path_list_ligand]


def extractSMILECODE (path_file_smi, debug = 0):
    
    
    filin = open (path_file_smi, "r")
    list_lines = filin.readlines ()
    filin.close ()
    
    smile_code = list_lines[0].split ("\t")[0]
    if debug : print smile_code, "SMILE CODE"
    
    return smile_code
    


def parseLigandDescriptor (path_file_list_ligand, path_file_descriptor_ligand, path_filout) : 
    
    filout_sum = open (path_filout, "w")
    filout_sum.write ("NameLigand\tDruggable\tfailuresLipinski\tLogP\tMW\tstate\n")
    p_filout_countDruggable = path_filout + "_fail_count"
    filout_countDruggable = open (p_filout_countDruggable, "w")
    d_drug_like = {}
    d_drug_like[0] = 0
    d_drug_like[1] = 0
    d_drug_like[2] = 0
    d_drug_like[3] = 0
    d_drug_like[4] = 0
    
    
    filin_list = open(path_file_list_ligand, "r")
    list_ligand = filin_list.readlines ()
    filin_list.close ()
    
    filin_descriptor = open (path_file_descriptor_ligand, "r")
    list_molecul_descriptor = filin_descriptor.readlines ()
    filin_descriptor.close ()
    
    
    nb_ligand = len (list_ligand)
    
    for i_ligand in xrange (0,nb_ligand) : 
        
        try : 
            a= list_molecul_descriptor[i_ligand+1].split ("\t")[5]
        except : 
            continue
        
        ligand = list_ligand[i_ligand].strip().split ("\t")
        
        if ligand[1] == "d" : 
            fail = int(list_molecul_descriptor[i_ligand+1].split ("\t")[5])
            d_drug_like[fail] = d_drug_like[fail] + 1
            if fail == 0:
                ok = "ok" 
        elif ligand[1] == "n" and int(list_molecul_descriptor[i_ligand + 1].split ("\t")[5]) > 0 : 
            ok = "ok"
        else :
            ok = "no"
            
        print i_ligand
        print list_molecul_descriptor[i_ligand]
        try : filout_sum.write (str (ligand[0]) + "\t" + str(ligand[1]) + "\t" + str(list_molecul_descriptor[i_ligand+1].split ("\t")[5]) + "\t" + str (list_molecul_descriptor[i_ligand+1].split ("\t")[1]) + "\t" + str (list_molecul_descriptor[i_ligand+1].split ("\t")[3]) + "\t" + str (ok) + "\n")
        except : continue
    
    #header 
    filout_countDruggable.write ("\t".join([str(i) for i in range (0,5)]) + "\n")
    # list of fails
    filout_countDruggable.write ("\t".join([str(d_drug_like[i]) for i in range (0,5)]) + "\n")
    filout_countDruggable.close ()
    
    runOtherProg.BarplotFailDruglike(p_filout_countDruggable)
    
    
    
       
    

