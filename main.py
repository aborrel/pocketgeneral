# personal module
from os import path, listdir, remove, system
from re import search
from shutil import copyfile


import globalFonction
import analysis
import runOtherProg
import tool
import loadDescriptors
import comparison
import rateQuality
import pathDirectory
import ligandDrugLike
import zipCompress
import writeFiles
import overlapPocket



#################################
#     Fpocket parameters        #
#################################

#analysis.fpocketParameters(dico_K.keys ()[0:50], run = 0, step = 0.1)

#### revoir la selection de variable pour plus performant


def main (name_dataset, protomol_type, pocket_retrive_type, pocket_remove = 0, criteria_train=0.0, criteria_loo = 0.0, criteria_CV = 0.0, criteria_test = 0.0, nb_desc_model = 3 ) : 
    
    #################
    # Download data #
    #################
    # case apo form for dataset, append others dataset apo in if condition
    if search ("prox", pocket_retrive_type) :  
        if name_dataset == "ApoForm138": 
            dico_dataset = globalFonction.calculDatasetDictionary(name_dataset, pocket_remove=1, genrate_files=0)
            dico_dataset = tool.selectOnlyTypeStructure(dico_dataset, "holo structure")
        else : 
            dico_dataset = globalFonction.calculDatasetDictionary(name_dataset, genrate_files = 0, pocket_remove = pocket_remove) ######
    else : 
        dico_dataset = globalFonction.calculDatasetDictionary(name_dataset, genrate_files = 0, pocket_remove = pocket_remove)
    
    
    #######################
    #  Analysis Data Set  #
    #######################
    # globalFonction.analysisDataset(dico_dataset, name_dataset)
    
    
    ############################
    # Run accesibility solvant #
    ############################
    # runOtherProg.globalNACCESS(dico_dataset, name_dataset)
    
    ######################
    # Pocket Estimation  #
    ######################
    globalFonction.pocketEstimation(name_dataset, dico_dataset, pocket_retrive_type, runFpocket = 0)


    #################################
    # Run Surflex generate protomol #
    #################################
##    globalFonction.generationProtomol (dico_dataset, pocket_retrive_type, name_dataset)


    ###############################
    #  Analysis pocket estimation #
    ###############################    
    globalFonction.analysisPocketEstimation(dico_dataset, name_dataset, pocket_retrive_type)
    
    ###############################
    #      RUN Descriptors        #
    ###############################
    dico_descriptors = globalFonction.retrieveGlobalDescriptors (pocket_retrive_type, protomol_type, dico_dataset, name_dataset, write_file = 1, calcul_descriptor = 1, option_separate = 1, compo_aa = 0)
    
#     print dico_descriptors.keys ()
#     print dico_descriptors["data"]
#     print dico_descriptors["Druggable"]["PDB"].index ("1D09")
#     print dico_descriptors["Druggable"]["data"]
    
#     print dico_descriptors["Druggable"]["data"][62], dico_descriptors["Druggable"]["PDB"][62]
    #del rugosity (incomplet)
#     try :
#         del dico_descriptors["Druggable"]["area"]["rugosity"]
#         del dico_descriptors["No-Druggable"]["area"]["rugosity"]
#     except : 
#         pass
##    
#    ########################
#    #      Analysis        #
#    ########################
    # run prediction perhaps make new function to separate visualization / prediction / correlation
    globalFonction.analysisGlobalDescriptor (dico_descriptors, name_dataset, pocket_retrive_type, ttest = 0, ACP = 1, LDAGlobal = 0, CART = 0, correlation = 0, selection_variable = 0, SVM = 0, histogram = 1, radomForest = 0, regLog = 0,  criteria_train = criteria_train, criteria_loo = criteria_loo, nb_desc_model = nb_desc_model, criteria_CV = criteria_CV,  criteria_test = criteria_test)
    

    #####################################
    #   Specific descriptors analysis   #
    #####################################
#    analysis.correlationVolumeVSVolumeFpocket(dico_descriptors, dico_dataset.keys (), name_dataset, pocket_retrive_type)
    

def model (name_dataset_train, name_dataset_test, pocket_retrieve_type, protomol_type, pvalue, cor_value):
    """
    A voir pour changer et avoir un truc plus propre
    """
    globalFonction.validation(name_dataset_train, name_dataset_test, pocket_retrieve_type, protomol_type, pvalue, cor_value)
    
   

#######################

def multiModelTest (path_dir_model, path_dir_result = "best", name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove = 0): 
    
    # define folder result
    name_folder_model = path.dirname(path_dir_model + ".txt").split ("/")[-1]
    
    if not path.exists(path_dir_result) : 
        pathDirectory.generatePath(path_dir_result)
    else : 
        system( "rm -f " + path_dir_result + "*" )
    

    
    # case apo form for dataset, append others dataset apo in if condition
    if pocket_retrive_type == "proximity" and name_dataset == "ApoForm138": 
        dico_dataset = globalFonction.calculDatasetDictionary(name_dataset,genrate_files=0, pocket_remove = 1, debug=0)
        dico_dataset = tool.selectOnlyTypeStructure(dico_dataset, "holo structure") 
    else : 
        dico_dataset = globalFonction.calculDatasetDictionary(name_dataset, genrate_files=0, pocket_remove = pocket_remove, debug=0) 
        
        
    # dictionnary with descriptors   
    dico_descriptors = globalFonction.retrieveGlobalDescriptors (pocket_retrive_type, "none", dico_dataset, name_dataset, write_file = 0, calcul_descriptor = 0, option_separate = 1)
    
    #####################
    # write data global #
    #####################
    path_file_descriptor_global = writeFiles.globalDescriptors(dico_descriptors, path_dir_result + "all_pocket.data")
    
    # case with train and test set
    if "data" in dico_descriptors.keys () : 
        if dico_descriptors["data"] != [] : 
            path_file_descriptor_separed = writeFiles.specificDescriptorbyData(dico_descriptors, "global", path_dir_result)
    
    
    # for average drugg score
    l_proba_train = []
    l_proba_test = []
    l_proba_loo = []
    
      
    l_file_model = listdir(path_dir_model)
    p_file_result = path_dir_result + "best.result"
    if "path_file_descriptor_separed" in locals() :
        p_file_result_train = path_dir_result + "best_train.result"
        p_file_result_test = path_dir_result + "best_test.result"
    # check exist ?
    if path.exists(p_file_result) : 
        remove(p_file_result)
 
    for p_file_model in l_file_model : 
        if search("Rdata", p_file_model) : 
            # move Rdata model in result folder
            copyfile (path_dir_model + p_file_model ,path_dir_result + p_file_model)
            
            # name out file
            n_model = p_file_model.split ("_")[0]
            name_desc = path.basename(path_file_descriptor_global)
            
            
            out_global = runOtherProg.predictLDA (path_dir_result + p_file_model, path_file_descriptor_global, p_file_result, p_out = path_dir_result + n_model + "_loo", plot = 1, LOO = 1)
            l_proba_loo.append (out_global[0])
            
            
            
            if "path_file_descriptor_separed" in locals() : 
                out_train = runOtherProg.predictLDA (path_dir_result + p_file_model, path_file_descriptor_separed[0], p_file_result_train, p_out = path_dir_result + n_model + "_" +  path.basename(path_file_descriptor_separed[0]), plot = 1)
                out_test = runOtherProg.predictLDA (path_dir_result + p_file_model, path_file_descriptor_separed[1], p_file_result_test, p_out = path_dir_result + n_model + "_" +  path.basename(path_file_descriptor_separed[1]), plot = 1)
                
                l_proba_train.append (out_train[0])
                l_proba_test.append (out_test[0])
         
         
    
    # plot quality        
    if "path_file_descriptor_separed" in locals() :
        globalFonction.bestmultiModelApo (p_file_result_test)
        globalFonction.bestmultiModelApo (p_file_result_train)
        
    #compute means of proba
    globalFonction.averageScoreDruggProba (l_proba_test, path_dir_result + "resume_test.Proba")
    globalFonction.averageScoreDruggProba (l_proba_train, path_dir_result + "resume_train.Proba")
    globalFonction.averageScoreDruggProba (l_proba_loo, path_dir_result + "resume_LOO.Proba")

    return path_dir_result



def applyModelEstimator (pr_preselected_model, l_estimators, name_dataset, pocket_retrive_type, name_dir_result) : 
    
    l_dir_est = []
    
    for estimator in l_estimators : 
    
        p_dir_result = multiModelTest (pr_preselected_model, path_dir_result = pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + estimator + "/" , name_dataset="krasowski", pocket_retrive_type = estimator, pocket_remove=0)
        print p_dir_result, "CHECK1"
        l_dir_est.append (p_dir_result)
        
    
    d_score = {}
    for dir_est in l_dir_est : 
        
        print dir_est, "CHECK2"
        est = dir_est.split ("/")[-2]
        print est, "CHECK3"
        
        d_score[est] = {}
        
        l_files = listdir(dir_est)
        for f in l_files : 
            if search ("_train.result_plot$", f) : 
                tool.appendPerf (dir_est + f, d_score[est], "TRAIN")
            elif search ("_test.result_plot$", f) : 
                tool.appendPerf (dir_est + f, d_score[est], "TEST")
        
    print d_score, "CHECK--6--"
        
    filin_acc = open (pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_perf_acc.txt", "w")
    filin_mcc = open (pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_perf_mcc.txt", "w")
    
    
    filin_acc.write ("\t".join(l_estimators) + "\n")
    filin_mcc.write ("\t".join(l_estimators) + "\n")
    
    l_model = sorted (d_score[pocket_retrive_type].keys ())        
    for model in l_model : 
        filin_acc.write (str(model))
        filin_mcc.write (str(model))
        for est in l_estimators :
            filin_acc.write ("\t" + d_score[est][model]["acc TEST"])
            filin_mcc.write ("\t" +d_score[est][model]["mcc TEST"] )
        filin_acc.write ("\n")
        filin_mcc.write ("\n")
    
    filin_acc.close ()
    filin_mcc.close ()
    
    runOtherProg.crossPerformance(pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_perf_acc.txt", debug = 1)
    runOtherProg.crossPerformance(pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_perf_mcc.txt", debug = 1)
    


    filout_rank_mcc = open (pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_rank_mcc.txt", "w")
    filout_rank_acc = open (pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_rank_acc.txt", "w")
    
    d_rank_mcc = computeRankModel (d_score, "mcc TEST")
    d_rank_acc = computeRankModel (d_score, "acc TEST")
    print d_rank_acc , "check3"
    print d_rank_mcc    

    filout_rank_acc.write ("\t".join(l_estimators) + "\n")
    filout_rank_mcc.write ("\t".join(l_estimators) + "\n")


    for model in l_model :
        filout_rank_mcc.write (str (model)) 
        filout_rank_acc.write (str (model)) 
        for est in l_estimators : 
            filout_rank_acc.write ("\t" + str(d_rank_acc[est][model]))
            filout_rank_mcc.write ("\t" + str(d_rank_mcc[est][model]))
        filout_rank_acc.write ("\n")
        filout_rank_mcc.write ("\n")


    filout_rank_acc.close ()
    filout_rank_mcc.close ()


    runOtherProg.crossRank(pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_rank_acc.txt", debug = 1)
    runOtherProg.crossRank(pathDirectory.result(name_dataset) + pocket_retrive_type + "/" + name_dir_result + "/" + "score_rank_mcc.txt", debug = 1)




def computeRankModel (d_score, key_rank):
    

    import scipy.stats as ss


    d_out = {}
    d_temp = {}
    for est in d_score.keys () : 
        print est
        d_out [est] = {}
        d_temp [est] = []
        for model in d_score[est].keys () : 
            print d_temp[est]
            d_temp[est].append (float(d_score[est][model][key_rank]))
            


    for est in d_score.keys () : 
        print est
        d_temp[est] = list(ss.rankdata(d_temp[est]))
        d_temp[est] = [int (f) for f in d_temp[est]]
        for model in d_score[est].keys () : 
            d_out[est][model] = d_temp[est][0]
            del d_temp[est][0]

    print d_temp
    print d_out

    return d_out



def anovaDataset (l_pocket_estimation, name_dataset):
    
    pr_result = pathDirectory.result("ANOVA/" + name_dataset)
    
    d_dataset = globalFonction.calculDatasetDictionary(name_dataset, pocket_remove=0)
    d_desc = {}
    l_file_desc = []
    for pocket_estimation in l_pocket_estimation : 
        d_desc  = globalFonction.retrieveGlobalDescriptors (pocket_estimation, "none", d_dataset, name_dataset, write_file = 1, calcul_descriptor = 0, option_separate = 1, compo_aa = 0)
        print d_desc
        p_file_pocket = pr_result + name_dataset + "_" + pocket_estimation
        l_file_desc.append (writeFiles.globalDescriptors(d_desc, p_file_pocket))
    
    runOtherProg.ANOVAOrTest (l_file_desc)
    
    
    




# for test #
d = {}
d["a"] = {}
d["b"] = {}
d["a"]["M1"] = {}
d["a"]["M2"] = {}

d["b"]["M1"] = {}
d["b"]["M2"] = {}



d["a"]["M1"]["acc TEST"] = 2.3
d["a"]["M1"]["mcc TEST"] = 992.3
d["a"]["M2"]["acc TEST"] = 18.3
d["a"]["M2"]["mcc TEST"] = 188.3



d["b"]["M1"]["acc TEST"] = 3.4
d["b"]["M2"]["acc TEST"] = 2.3
d["b"]["M1"]["mcc TEST"] = 3.4
d["b"]["M2"]["mcc TEST"] = 2.3

#print d

#computeRankModel (d, "acc")

#applyModelEstimator ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_3/BestModels0.65_0.65/", ["a","b"], "krasowski", "proximity", "test", d)

      
       


data_set = "krasowski"
#main (data_set, "surflexe", "proximity")
# main (data_set, "surflexe", "Fpocket")
# 
# main (data_set, "none", "prox4.0")
# main (data_set, "none", "prox5.5")
# main (data_set, "none", "Fpocket")
# main (data_set, "none", "DogSite")
# 
# main ("ApoForm138", "none", "Fpocket")
# main ("ApoForm138", "none", "DogSite")


data_set = "Perola"
#main (data_set, "none", "prox4.0")
# main (data_set, "none", "prox5.0")
# main (data_set, "none", "prox5.5")
# main (data_set, "none", "Fpocket")




#################
##  PROX-4.0   ##
#################

# main (data_set, "none", "prox4.0", criteria_train=0.56, criteria_loo = 0.56, criteria_CV = 0.57, nb_desc_model = 2)
# main (data_set, "none", "prox4.0", criteria_train=0.60, criteria_loo = 0.61, criteria_CV = 0.60, nb_desc_model = 3)
# main (data_set, "none", "prox4.0", criteria_train=0.66, criteria_loo = 0.604, criteria_CV = 0.547,criteria_test = 0.80, nb_desc_model = 4)
# main (data_set, "none", "prox4.0", criteria_train=0.90, criteria_loo = 0.80, nb_desc_model = 5)

#################
##  PROX-5.0   ##
#################

# main (data_set, "none", "prox5.0", criteria_train=0.56, criteria_loo = 0.56, criteria_CV = 0.57, nb_desc_model = 2)
# main (data_set, "none", "prox5.0", criteria_train=0.60, criteria_loo = 0.61, criteria_CV = 0.60, nb_desc_model = 3)
# main (data_set, "none", "prox5.0", criteria_train=0.66, criteria_loo = 0.604, criteria_CV = 0.547,criteria_test = 0.80, nb_desc_model = 4)

#################
##  PROX-5.5   ##
#################

# main (data_set, "none", "prox5.5", criteria_train=0.56, criteria_loo = 0.56, criteria_CV = 0.58, criteria_test = 0.60, nb_desc_model = 2)
# main (data_set, "none", "prox5.5", criteria_train = 0.65, criteria_loo = 0.65, criteria_CV = 0.68, criteria_test = 0.50, nb_desc_model = 3)
# main (data_set, "none", "prox5.5", criteria_train=0.60, criteria_loo = 0.60, criteria_CV = 0.50, criteria_test = 0.72, nb_desc_model = 4)
# main (data_set, "none", "prox5.5", criteria_train=0.70, criteria_loo = 0.70, criteria_CV = 0.58, criteria_test = 0.72, nb_desc_model = 5)

#################
##  PROX-6.0   ##
#################

# main (data_set, "none", "prox6.0", criteria_train=0.60, criteria_loo = 0.60, criteria_CV = 0.60, nb_desc_model = 2)
# main (data_set, "none", "prox6.0", criteria_train=0.65, criteria_loo = 0.65, criteria_CV = 0.66, nb_desc_model = 3)
# main (data_set, "none", "prox6.0", criteria_train=0.60, criteria_loo = 0.60, criteria_CV = 0.00, criteria_test = 0.76, nb_desc_model = 4)
# main (data_set, "none", "prox6.0", criteria_train=0.90, criteria_loo = 0.80, nb_desc_model = 5)

#################
##   Fpocket   ##
#################

# main (data_set, "none", "Fpocket", criteria_train =0.60, criteria_loo = 0.60, criteria_CV = 0.63, nb_desc_model = 2)
# main (data_set, "none", "Fpocket", criteria_train=0.644, criteria_loo = 0.593, criteria_CV = 0.56, criteria_test = 0.55, nb_desc_model = 3)
# main (data_set, "none", "Fpocket", criteria_train=0.70, criteria_loo = 0.650, criteria_CV = 0.65, criteria_test = 0.65, nb_desc_model = 4)
# main (data_set, "none", "Fpocket", criteria_train=0.70, criteria_loo = 0.65, criteria_CV = 0.65, nb_desc_model = 5)



###################
##   cavitator   ##
###################

# main (data_set, "none", "cavitator")

#Case with dogsite -> unzio the pocket folder
# zipCompress.unCompressFolder("/home/borrel/druggabilityProject/DOGSITE/")

###################
##   DogSite     ##
###################

# main (data_set, "none", "DogSite", criteria_train=0.60, criteria_loo = 0.60, nb_desc_model = 2)
# main (data_set, "none", "DogSite", criteria_train=0.70, criteria_loo = 0.70, nb_desc_model = 3)
# main (data_set, "none", "DogSite", criteria_train=0.80, criteria_loo = 0.70, nb_desc_model = 4)
# main (data_set, "none", "DogSite", criteria_train=0.75, criteria_loo = 0.70, nb_desc_model = 5)



# data_set = "DD"
# main (data_set, "none", "Fpocket")
# main (data_set, "none", "prox4.0")

# data_set = "Perola"

#main (data_set, "none", "Fpocket")
#main (data_set, "surflexe", "prox4.0")




#################################
# selection model via estimator #
#################################


#############
#  Fpocket  #
#############

# l_estimator = ["prox5.5","Fpocket", "DogSite"]

# applyModelEstimator ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels0.7_0.65_0.65_0.65/", l_estimator, "krasowski", "Fpocket", "selectModelPPE4-12-12-2014")

# applyModelEstimator ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels0.644_0.593_0.56_0.55/", l_estimator, "krasowski", "Fpocket", "selectPPE3")
# applyModelEstimator ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels0.7_0.65_0.6_0.55/", l_estimator, "krasowski", "Fpocket", "selectPPE4")






############
# PROX-5.5 #
############

# l_estimator = ["prox5.5", "prox4.0", "Fpocket", "DogSite"]
# applyModelEstimator ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels0.6_0.6_0.5_0.72/", l_estimator, "krasowski", "prox5.5", "selectPPE4-0.60_0.60_0.5_0.72__11-12-2014")



#####################
# Apply best models #
#####################


# Test New with LOO on only train

####OPE#####
############
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE_4desc_5models-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE_4desc_5models-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/selectedDesc_4/BestModels/","/home/borrel/druggabilityProject/result/krasowski/proximity/OPE_4desc_5models-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "proximity", pocket_remove=0)


####PPE####
###########
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/DogSite/PPE_3desc_7models-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/PPE_3desc_7models-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/proximity/PPE_3desc_7models-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "proximity", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/prox6/PPE_3desc_7models-08-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "prox6", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/prox5.5/pockDrug-11-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "prox5.5", pocket_remove=0)

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels-1/","/home/borrel/druggabilityProject/result/krasowski/prox5.5/PPE4-12-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "prox5.5", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels-1/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/PPE4-12-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_4/BestModels-1/","/home/borrel/druggabilityProject/result/krasowski/DogSite/PPE4-12-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)


####OPE-prox6####
#################
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE6_4desc_6models-08-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE6_4desc_6models-08-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-2/","/home/borrel/druggabilityProject/result/krasowski/prox6.0/OPE6_4desc_6models-08-12-2014-2/" , name_dataset="krasowski", pocket_retrive_type = "prox6.0", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-3/","/home/borrel/druggabilityProject/result/krasowski/prox6.0/OPE6_4desc_6models-08-12-2014-3/" , name_dataset="krasowski", pocket_retrive_type = "prox6.0", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-5/","/home/borrel/druggabilityProject/result/krasowski/prox6.0/OPE6_4desc_6models-08-12-2014-5/" , name_dataset="krasowski", pocket_retrive_type = "prox6.0", pocket_remove=0)

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE6_3desc_5model-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE6_3desc_5model-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_3/BestModels/","/home/borrel/druggabilityProject/result/krasowski/prox6.0/OPE6_3desc_5model-04-12-2014/" , name_dataset="krasowski", pocket_retrive_type = "prox6.0", pocket_remove=0)


# prox6 - best5

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-5/","/home/borrel/druggabilityProject/result/krasowski/prox6.0/OPE6_4desc_6models-08-12-2014-5/" , name_dataset="krasowski", pocket_retrive_type = "prox6.0", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-5/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE6_4desc_6models-08-12-2014-5/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-5/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE6_4desc_6models-08-12-2014-5/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-6-2/","/home/borrel/druggabilityProject/result/krasowski/prox6.0/OPE6_4desc_6models-09-12-2014-6-2/" , name_dataset="krasowski", pocket_retrive_type = "prox6.0", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-6-2/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE6_4desc_6models-09-12-2014-6-2/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox6.0/LDA/selectedDesc_4/BestModels-6-2/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE6_4desc_6models-09-12-2014-6-2/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)


###################
##  OPE-prox5.5  ##
###################

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-1/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE5.5_4desc-11-12-2014-1/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-1/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE5.5_4desc-11-12-2014-1/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-1/","/home/borrel/druggabilityProject/result/krasowski/prox5.5/OPE5.5_4desc-11-12-2014-1/" , name_dataset="krasowski", pocket_retrive_type = "prox5.5", pocket_remove=0)

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-2/","/home/borrel/druggabilityProject/result/krasowski/DogSite/OPE5.5_4desc-11-12-2014-2/" , name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-2/","/home/borrel/druggabilityProject/result/krasowski/Fpocket/OPE5.5_4desc-11-12-2014-2/" , name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-2/","/home/borrel/druggabilityProject/result/krasowski/prox5.5/OPE5.5_4desc-11-12-2014-2/" , name_dataset="krasowski", pocket_retrive_type = "prox5.5", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/prox5.5/LDA/selectedDesc_4/BestModels-2/","/home/borrel/druggabilityProject/result/krasowski/prox4.0/OPE5.5_4desc-11-12-2014-2/" , name_dataset="krasowski", pocket_retrive_type = "prox4.0", pocket_remove=0)



######################
# DRUGGABILITY MODEL #
######################


########################
## PPE-2 descriptors  ##
########################
# 
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_2/BestModels/", "PPE2_bestModel-Prox", name_dataset="krasowski", pocket_retrive_type = "prox4.0", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_2/BestModels/", "PPE2_bestModel-fpocket", name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/selectedDesc_2/BestModels/", "PPE2_bestModel-dogsite", name_dataset="krasowski", pocket_retrive_type = "DogSite", pocket_remove=0)



#######################
##    Cross model    ##
## cross descriptors ##
#######################

# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/hydrophobicity_geometry/", "/home/borrel/druggabilityProject/result/krasowski/Fpocket/Cross_hydophobicity_geometry/", name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/hydrophobicity_interaction/", "/home/borrel/druggabilityProject/result/krasowski/Fpocket/Cross_hydophobicity_interaction/", name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)
# multiModelTest ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/interaction_geometry/", "/home/borrel/druggabilityProject/result/krasowski/Fpocket/Cross_interaction_geometry/", name_dataset="krasowski", pocket_retrive_type = "Fpocket", pocket_remove=0)

###############cross analysis###############
############################################

# ACP with 2 dataset -> 16-10 change fonction
# descriptor selected
#globalFonction.ACPDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "Fpocket", "none", list_descriptor = ["p_Ooh_atom", "hydrophobic_kyte", "RADIUS_CYLINDER", "PCI", "p_aromatic_residues"])
#globalFonction.ACPDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "Fpocket", "none", list_descriptor = ["RADIUS_HULL", "DIAMETER_HULL", "SURFACE_HULL", "VOLUME_HULL", "SMALLEST_SIZE", "INERTIA_3", "INERTIA_1", "FACE", "HEIGHT_CYLINDER", "PCI", "PSI", "RADIUS_CYLINDER", "%_ATOM_CONVEXE", "CONVEX-SHAPE_COEFFICIENT", "INERTIA_2", "c_residues", "c_atom", "Real_volume"])
#globalFonction.ACPDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "Fpocket", "none", list_descriptor = ["RADIUS_HULL", "DIAMETER_HULL", "SURFACE_HULL", "VOLUME_HULL", "SMALLEST_SIZE", "INERTIA_3", "INERTIA_1", "FACE", "HEIGHT_CYLINDER", "PCI", "PSI", "RADIUS_CYLINDER", "%_ATOM_CONVEXE", "CONVEX-SHAPE_COEFFICIENT", "INERTIA_2", "c_residues", "c_atom"])


# All descriptors
# globalFonction.ACPDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "Fpocket", "none")
# globalFonction.MDSDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "Fpocket", "none")
# globalFonction.correlationDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "Fpocket", "none")
#  
# globalFonction.ACPDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "DogSite", "none")
# globalFonction.MDSDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "DogSite", "none")
# globalFonction.correlationDataset1BetwwenDataset2 ("krasowski", "krasowski", "prox4.0", "none", "DogSite", "none")
#  
# globalFonction.ACPDataset1BetwwenDataset2 ("krasowski", "krasowski", "DogSite", "none", "Fpocket", "none")
# globalFonction.MDSDataset1BetwwenDataset2 ("krasowski", "krasowski", "DogSite", "none", "Fpocket", "none")
# globalFonction.correlationDataset1BetwwenDataset2 ("krasowski", "krasowski", "DogSite", "none", "Fpocket", "none")
#
################# 
# ACP 3 dataset #
#################
# globalFonction.ACPDatasetBetwwen3Dataset ("krasowski", "krasowski","krasowski", "DogSite", "none", "Fpocket", "none", "prox5.5", "none")


################# 
# ACP 4 dataset #
#################
# globalFonction.ACPDatasetBetween4Dataset ("krasowski", "krasowski","krasowski","krasowski","DogSite", "none", "Fpocket", "none", "prox4.0", "none", "prox5.5", "none" )

###############
#   ANOVA     #
###############
# anova or means comparison with a unique dataset diferently estimated
# anovaDataset (["prox4.0", "prox5.5", "Fpocket", "DogSite"], "krasowski")
# anovaDataset (["Fpocket", "DogSite"], "ApoForm138")


#################
## Correlation ##
#################

# correlation with 2 types estimation
#
################
# obs VS pred  #
################

# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "DogSite", "none",  "Fpocket", "none" )
# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "Fpocket", "none",  "prox5.5", "none" )
# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "DogSite", "none",  "prox5.5", "none" )
# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "DogSite", "none",  "prox4.0", "none" )
# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "Fpocket", "none",  "prox4.0", "none" )

#######################
# observed thresold   #
#######################

# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "prox5.5", "none",  "prox4.0", "none" )
# globalFonction.correlationDescriptorsByTypePocket ("krasowski", "prox5.5", "none",  "prox6.0", "none" )



### Proximity ####
##################
#globalFonction.correlationDescriptor("krasowski", ["hydrophobic_kyte", "aromatic_residues"],"proximity", "none")
#globalFonction.correlationDescriptor("krasowski", ["hydrophobic_kyte", "PCI"],"proximity", "none")
#globalFonction.correlationDescriptor("krasowski", ["hydrophobic_kyte", "RADIUS_CYLINDER"],"proximity", "none")
#
#globalFonction.correlationDescriptor("krasowski", ["RADIUS_CYLINDER", "PCI"],"proximity", "none")
#globalFonction.correlationDescriptor("krasowski", ["aromatic_residues", "RADIUS_CYLINDER"],"proximity", "none")

### Fpocket ###
###############
#globalFonction.correlationDescriptor("krasowski", ["hydrophobic_kyte", "aromatic_residues"],"Fpocket", "none")
#globalFonction.correlationDescriptor("krasowski", ["hydrophobic_kyte", "alcool"],"Fpocket", "none")


###########################
# FPOCKET - DogSite SCORE #
###########################

# rateQuality.qualityEstimator("Fpocket", "krasowski", pathDirectory.result("Score_Fpocket") + "quality_Fpocket_krasowski")
# rateQuality.qualityEstimator("Fpocket", "ApoForm138", pathDirectory.result("Score_Fpocket") + "quality_Fpocket_apo138")
#    
# rateQuality.qualityEstimator("DogSite", "krasowski", pathDirectory.result("Score_DogSite") + "quality_DogSite_krasowski")
# rateQuality.qualityEstimator("DogSite", "ApoForm138", pathDirectory.result("Score_DogSite") + "quality_DogSite_apo138")


################
# comparison   #
################

# comparison.comparisonWithLigandWithoutLigand("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/WithoutLigand","/home/borrel/druggabilityProject/result/krasowski/proximity/LDA/WithLigand")
# comparison.FpocketWithoutLigand ("/home/borrel/druggabilityProject/result/krasowski/Fpocket/LDA/WithoutLigand", "krasowski", "Fpocket", "none")


####################
#  LIPINSKI RULES  #
####################
# dico_dataset = globalFonction.calculDatasetDictionary("krasowski",0)
# path_file_list_ligand = ligandDrugLike.retrieveSmileDrugLike (dico_dataset, pathDirectory.dataSet("krasowski") + "ligandAnalysis") [1] # list files
# After weeb service http://crdd.osdd.net:8081/webcdk/desc.jsp
path_file_list_ligand = "/home/borrel/druggabilityProject/dataSet/krasowski/ligandAnalysis.list"
#
path_ligand_druglike = ligandDrugLike.parseLigandDescriptor (path_file_list_ligand, "/home/borrel/druggabilityProject/dataSet/krasowski/ligand_descripteur.txt", "/home/borrel/druggabilityProject/dataSet/krasowski/ligand_lipinski")


#####################
#  OVERLAAP POCKET  #
#####################

# globalFonction.overlapTwoPockets("krasowski", "Fpocket", "proximity")
# globalFonction.overlapTwoPockets("krasowski", "DogSite", "proximity")
# globalFonction.overlapTwoPockets("krasowski", "DogSite", "Fpocket")
# globalFonction.overlapTwoPockets("ApoForm138", "DogSite", "Fpocket")
# 
# globalFonction.overlapTwoPockets("krasowski", "prox4.0", "prox6.0")
# globalFonction.overlapTwoPockets("krasowski", "DogSite", "prox6.0")
# globalFonction.overlapTwoPockets("krasowski", "Fpocket", "prox6")
#
# globalFonction.overlapTwoPockets("krasowski", "prox4.0", "prox6.0")
# globalFonction.overlapTwoPockets("krasowski", "prox5.5", "prox4.0")
# globalFonction.overlapTwoPockets("krasowski", "prox5.5", "prox6.0")
# globalFonction.overlapTwoPockets("krasowski", "prox5.5", "Fpocket")
# globalFonction.overlapTwoPockets("krasowski", "prox5.5", "DogSite")

# globalFonction.overlapTwoPockets("krasowski", "DogSite", "prox4.0")
# globalFonction.overlapTwoPockets("krasowski", "Fpocket", "prox4.0")

# globalFonction.overlapTwoPockets("krasowski", "DogSite", "prox5.5")
# globalFonction.overlapTwoPockets("krasowski", "Fpocket", "prox5.5")
# globalFonction.overlapTwoPockets("krasowski", "DogSite", "Fpocket")

#########################
#  Accesibility Pocket  #
#########################

# globalFonction.retrieveAccessibilityPocket("krasowski", "proximity")


########################
#   Overlap analysis   #
########################

# atention -> pocket estimation !!!!
# overlapPocket.overlapAnalysis ([x * 0.5 for x in range(6,15)], "krasowski",select_pocket_based = "proximity" )






